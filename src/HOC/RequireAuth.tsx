import { useTypedSelector } from "hooks/useTypedSelector";
import React from "react";
import { Navigate, useLocation } from "react-router-dom";

export interface IRequireAuth {
  children: React.ReactNode;
}

const RequireAuth: React.FC<IRequireAuth> = ({ children }) => {
  const { pathname } = useLocation();
  console.log("pathname", pathname);

  const { loggedIn } = useTypedSelector((state) => state.loggedInState);
  const { company_name } = useTypedSelector(
    (state) => state.sessionState.session
  );
  const { items: projects } = useTypedSelector((state) => state.projectState);

  if (pathname === "/login" || pathname === "/") {
    if (!loggedIn) {
      return <>{children}</>;
    }
    return <Navigate to={`/${company_name}/projects`} />;
  }

  if (loggedIn) {
    return <>{children}</>;
  }
  return <Navigate to="/login" />;
};

export default RequireAuth;
