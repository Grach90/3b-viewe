export const login = (user) =>
  fetch(`/api/v1.0.0/sessions`, {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    },
    credentials: "same-origin",
  });

export const signup = (user) =>
  fetch(`/api/v1.0.0/users`, {
    method: "POST",
    body: JSON.stringify(user),
    headers: {
      "Content-Type": "application/json",
    },
  });

export const logout = () =>
  fetch(`/api/v1.0.0/sessions`, {
    method: "DELETE",
  });

export const checkLoggedIn = async () => {
  try {
    const response = await fetch(`/api/v1.0.0/sessions`);
    const data = await response.json();
    console.log("data", data);
    if (data.status === 403) throw data.message;
    return {
      sessionState: { session: data.session.user },
      loggedInState: { loggedIn: true },
    };
  } catch (e) {
    console.log(e);
    // alert("Something went wrong, please try againt");
  }
};
