export const fetchProjects = (company_id: string) =>
  fetch(`/api/v1.0.0/companies/${company_id}/projects`);

export const createProject = (company_id: string, project_name: string) =>
  fetch(`/api/v1.0.0/companies/${company_id}/projects`, {
    method: "POST",
    body: JSON.stringify({ project_name }),
    headers: {
      "Content-Type": "application/json",
    },
  });

export const updateProject = (
  company_id: string,
  new_name: string,
  project_id: string
) =>
  fetch(`/api/v1.0.0/companies/${company_id}/projects/${project_id}`, {
    method: "PATCH",
    body: JSON.stringify({ new_name }),
    headers: {
      "Content-Type": "application/json",
    },
  });

export const deleteProject = (company_id: string, checkedProjects: []) =>
  fetch(
    `/api/v1.0.0/companies/${company_id}/projects?ids=${checkedProjects.join(
      ","
    )}`,
    {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

// export const getProjectConfigFile = async (company_name, project_name) => {
//   try {
//     return await fetch(
//       `/api/companies/${company_name}/projects/${project_name}`,
//     );
//   } catch (err) {
//     return {
//       status: "error",
//       message: err,
//     };
//   }
// };

// export const receiveRights = async (user_id, project_id) => {
//   try {
//     return fetch(`/api/users/${user_id}/projects/${project_id}/permission`);
//   } catch (err) {
//     return {
//       status: "error",
//       message: err,
//     };
//   }
// };
