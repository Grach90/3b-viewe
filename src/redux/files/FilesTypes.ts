export enum FilesActionTypes {
  RECEIVE_ALL_FILES = "RECEIVE_ALL_FILES",
  ADD_CHECKED_FILES = "ADD_CHECKED_FILES",
  ALL_CHECKED_FILES = "ALL_CHECKED_FILES",
}

export interface IFiles {
  file_name: string;
  extension: string;
  version: number;
  file_size: number;
  uploader_email: string;
  created_at: string;
}

interface IAttachedFiles {
  [key: string]: IFiles;
}

export interface IListFiles {
  id: string;
  file_name: string;
  extension: string;
  version: number;
  file_size: number;
  uploader_email: string;
  created_at: string;
  attached_files: IAttachedFiles;
}

export interface IInitStates {
  list_files: Array<IListFiles>;
  checkedArr: Array<string>;
  in_progress: Array<string>;
  downloadFile: object;
  allchecked: boolean;
  isFetching: boolean;
  format_file: string;
  viewType: string;
  search: string;
  changeValue: number;
  list_upload_canceling_files: object;
  error_during_uploading: [];
  bool_for_spin_during_canceling_upload: object;
  failed_something: {
    bool: boolean;
    message: string;
  };
}

interface ReceiveAllFilesAction {
  type: FilesActionTypes.RECEIVE_ALL_FILES;
  payload: Array<IListFiles>;
}

interface CheckedFilesAction {
  type: FilesActionTypes.ADD_CHECKED_FILES;
  payload: string;
}

interface AllCheckedFilesAction {
  type: FilesActionTypes.ALL_CHECKED_FILES;
}

export type FilesAction =
  | ReceiveAllFilesAction
  | CheckedFilesAction
  | AllCheckedFilesAction;
