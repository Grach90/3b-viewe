import { IInitStates, FilesAction, FilesActionTypes } from "./FilesTypes";
const files = [
  {
    id: "6f460f53-cd82-4a0f-b701-e93232cccf90",
    file_name: "Test",
    extension: "rvt",
    version: 1,
    file_size: 423562,
    uploader_email: "s.kirakosyan@archidutch.am",
    created_at: "2022-03-10T07:29:44.791Z",
    attached_files: {
      "970880ad-085d-4735-bfd9-870d62c4c1f7": {
        file_name: "TestIFC",
        extension: "ifc",
        version: 1,
        file_size: 423562,
        uploader_email: "s.kirakosyan@archidutch.am",
        created_at: "2022-03-10T07:29:44.791Z",
      },
      "d1092292-a3ca-4f82-8e5b-19b371dcd1ba": {
        file_name: "Test",
        extension: "obj",
        version: 1,
        file_size: 423562,
        uploader_email: "s.kirakosyan@archidutch.am",
        created_at: "2022-03-10T07:29:44.791Z",
      },
    },
  },
  {
    id: "6f460f53-cd82-4a0f-b701-e93232cccf91",
    file_name: "Test",
    extension: "rvt",
    version: 1,
    file_size: 423562,
    uploader_email: "s.kirakosyan@archidutch.am",
    created_at: "2022-03-10T07:29:44.791Z",
    attached_files: {
      "970880ad-085d-4735-bfd9-870d62c4c1f7": {
        file_name: "TestIFC",
        extension: "ifc",
        version: 1,
        file_size: 423562,
        uploader_email: "s.kirakosyan@archidutch.am",
        created_at: "2022-03-10T07:29:44.791Z",
      },
      "d1092292-a3ca-4f82-8e5b-19b371dcd1ba": {
        file_name: "Test",
        extension: "obj",
        version: 1,
        file_size: 423562,
        uploader_email: "s.kirakosyan@archidutch.am",
        created_at: "2022-03-10T07:29:44.791Z",
      },
    },
  },
];

const initState: IInitStates = {
  list_files: files,
  checkedArr: [],
  in_progress: [],
  downloadFile: {},
  allchecked: false,
  isFetching: true,
  format_file: "",
  viewType: "",
  search: "",
  changeValue: 0,
  list_upload_canceling_files: {},
  error_during_uploading: [],
  bool_for_spin_during_canceling_upload: {},
  failed_something: {
    bool: false,
    message: "",
  },
};

const FilesReducer = (state = initState, action: FilesAction): IInitStates => {
  switch (action.type) {
    case FilesActionTypes.RECEIVE_ALL_FILES: {
      return {
        ...state,
        list_files: action.payload,
      };
    }
    case FilesActionTypes.ADD_CHECKED_FILES: {
      const bool = state.checkedArr.find((item) => item === action.payload);
      let arr: Array<string> = state.checkedArr;
      if (bool) {
        arr = state.checkedArr.filter((item) => item !== action.payload);
      } else {
        arr.push(action.payload);
      }
      return {
        ...state,
        checkedArr: arr,
      };
    }
    case FilesActionTypes.ALL_CHECKED_FILES: {
      return {
        ...state,
        checkedArr: state.allchecked
          ? []
          : state.list_files.map((item) => item.id),
        allchecked: !state.allchecked,
      };
    }
    default:
      return state;
  }
};

export default FilesReducer;
