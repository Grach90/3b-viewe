import { FilesActionTypes, FilesAction, IListFiles } from "./FilesTypes";

export const ReceiveAllFiles = (files: Array<IListFiles>): FilesAction => ({
  type: FilesActionTypes.RECEIVE_ALL_FILES,
  payload: files,
});

export const CheckedFiles = (fileId: string): FilesAction => ({
  type: FilesActionTypes.ADD_CHECKED_FILES,
  payload: fileId,
});

export const AllCheckedFiles = (): FilesAction => ({
  type: FilesActionTypes.ALL_CHECKED_FILES,
});
