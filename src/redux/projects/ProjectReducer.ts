import { IinitState, ProjectActionTypes, ActionType } from "./ProjectTypes";

const initState: IinitState = {
  checkedProjects: [],
  items: [],
  user_permissions: {},
  isFetching: true,
  delete: 0,
  bool_for_redirect_to_new_project: false,
  removeButtons: false,
  // failed_something_in_projects: {
  //   bool: false,
  //   message: "",
  // },
};

const ProjectReducer = (
  state: IinitState = initState,
  action: ActionType
): IinitState => {
  switch (action.type) {
    case ProjectActionTypes.CHANGE_IS_FETCHING: {
      return {
        ...state,
        isFetching: action.payload,
      };
    }
    case ProjectActionTypes.RECEIVE_ALL_PROJECTS: {
      return {
        ...state,
        items: action.payload,
      };
    }
    case ProjectActionTypes.RECIEVE_UPDATED_PROJECT: {
      return {
        ...state,
        items: state.items.map((item) => {
          if (item.id === action.payload.project_id) {
            return {
              ...item,
              name: action.payload.new_name,
            };
          } else return item;
        }),
      };
    }
    case ProjectActionTypes.CHANGE_REMOVE_BUTTONS: {
      return {
        ...state,
        removeButtons: action.payload,
      };
    }
    case ProjectActionTypes.RESET_CHECKED_PROJECTS: {
      return {
        ...state,
        checkedProjects: [],
      };
    }
    case ProjectActionTypes.ADD_CHECKED_PROJECTS: {
      const bool = state.checkedProjects.find(
        (item) => item === action.payload
      );
      let arr: Array<string> = state.checkedProjects;
      if (bool) {
        arr = state.checkedProjects.filter((item) => item !== action.payload);
      } else {
        arr.push(action.payload);
      }
      return {
        ...state,
        checkedProjects: arr,
      };
    }
    case ProjectActionTypes.DELETE_PROJECTS: {
      return {
        ...state,
        checkedProjects: [],
        items: state.items.filter(
          (i) => !action.payload.find((x) => i.id === x)
        ),
      };
    }
    default:
      return state;
  }
};

export default ProjectReducer;
