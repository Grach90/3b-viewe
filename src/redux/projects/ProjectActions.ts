import {
  ActionType,
  ProjectActionTypes,
  IProject,
  IUpdatedProject,
} from "./ProjectTypes";
import { Dispatch } from "redux";
import * as apiProject from "../../api/project";

const changeIsFettching = (isfetching: boolean): ActionType => ({
  type: ProjectActionTypes.CHANGE_IS_FETCHING,
  payload: isfetching,
});

const receiveProjects = (data: Array<IProject>): ActionType => ({
  type: ProjectActionTypes.RECEIVE_ALL_PROJECTS,
  payload: data,
});

const addProjects = (data: IProject): ActionType => ({
  type: ProjectActionTypes.ADD_PROJECT,
  payload: data,
});

const redirect_to_project = (bool: boolean): ActionType => ({
  type: ProjectActionTypes.REDIRECT_TO_NEW_PROJECT,
  payload: bool,
});

const receiveUpdatedProject = (data: IUpdatedProject): ActionType => ({
  type: ProjectActionTypes.RECIEVE_UPDATED_PROJECT,
  payload: data,
});

const changeRemoveButton = (bool: boolean): ActionType => ({
  type: ProjectActionTypes.CHANGE_REMOVE_BUTTONS,
  payload: bool,
});

const resetCheckedProjects = (): ActionType => ({
  type: ProjectActionTypes.RESET_CHECKED_PROJECTS,
});

const addCheckedProjects = (id: string): ActionType => ({
  type: ProjectActionTypes.ADD_CHECKED_PROJECTS,
  payload: id,
});

const deletePojects = (array: []): ActionType => ({
  type: ProjectActionTypes.DELETE_PROJECTS,
  payload: array,
});

export const fetchProjects =
  (company_id: string) => async (dispatch: Dispatch<ActionType>) => {
    try {
      // dispatch(receiveProjects([]));
      dispatch(changeIsFettching(true));

      const response = await apiProject.fetchProjects(company_id);
      const data = await response.json();
      console.log("data", data);

      if (data.status >= 400) throw data.message;
      dispatch(changeIsFettching(false));
      dispatch(receiveProjects(data));
    } catch (err) {
      dispatch(changeIsFettching(false));
      console.log("err", err);
    }
  };

export const createProject =
  (company_id: string, project_name: string) =>
  async (dispatch: Dispatch<ActionType>) => {
    console.log(project_name);

    try {
      const response = await apiProject.createProject(company_id, project_name);

      const data = await response.json();
      if (data.status >= 400) throw data.message;

      dispatch(addProjects(data));
      dispatch(redirect_to_project(true));
    } catch (err) {
      console.log("err", err);
    }
  };

export const updateProject =
  (company_id: string, project_id: string, new_name: string) =>
  async (dispatch: Dispatch<ActionType>) => {
    try {
      const response = await apiProject.updateProject(
        company_id,
        project_id,
        new_name
      );
      const data = await response.json();
      if (data.status >= 400) throw data.message;
      dispatch(receiveUpdatedProject({ new_name: data.new_name, project_id }));
    } catch (err) {
      console.log("err", err);
    }
  };
export const deleteProjects =
  (company_id: string, checkedProjects: []) =>
  async (dispatch: Dispatch<ActionType>) => {
    try {
      const response = await apiProject.deleteProject(
        company_id,
        checkedProjects
      );
      console.log("deletresponse", response);

      // const data = await response.json();
      // console.log("deletData", data);
      if (response.status >= 400) throw response.statusText;
      dispatch(deletePojects(checkedProjects));
      // dispatch(resetCheckedProjects());
      // fetchProjects(company_id);
    } catch (err) {
      console.log("err", err);
    }
  };

export const changeValueOfRemoveButton =
  (bool: boolean) => (dispatch: Dispatch<ActionType>) =>
    dispatch(changeRemoveButton(bool));

export const emptyCheckedArr = () => (dispatch: Dispatch<ActionType>) => {
  dispatch(resetCheckedProjects());
  dispatch(changeRemoveButton(false));
};

export const addProjectInCheckedArr =
  (id: string) => (dispatch: Dispatch<ActionType>) =>
    dispatch(addCheckedProjects(id));
