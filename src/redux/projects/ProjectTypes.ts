export enum ProjectActionTypes {
  CHANGE_IS_FETCHING = "CHANGE_IS_FETCHING",
  RECEIVE_ALL_PROJECTS = "RECEIVE_ALL_PROJECTS",
  ADD_PROJECT = "ADD_PROJECT",
  REDIRECT_TO_NEW_PROJECT = "REDIRECT_TO_NEW_PROJECT",
  RECIEVE_UPDATED_PROJECT = "RECIEVE_UPDATED_PROJECT",
  CHANGE_REMOVE_BUTTONS = "CHANGE_REMOVE_BUTTONS",
  RESET_CHECKED_PROJECTS = "RESET_CHECKED_PROJECTS",
  ADD_CHECKED_PROJECTS = "ADD_CHECKED_PROJECTS",
  DELETE_PROJECTS = "DELETE_PROJECTS",
}

export interface IinitState {
  checkedProjects: Array<string>;
  items: Array<IProject>;
  user_permissions: Ipermissions | object;
  isFetching: boolean;
  delete: number;
  bool_for_redirect_to_new_project: boolean;
  removeButtons: boolean;
}

export interface IProject {
  id: string;
  name: string;
  is_active: boolean;
  is_private: boolean;
}

export interface IUpdatedProject {
  new_name: string;
  project_id: string;
}

interface Ipermissions {
  id: string;
  project_user_id: string;
  can_show_project: boolean;
  can_upload_file: boolean;
  can_delete_file: boolean;
  can_open_file_in_viewer: boolean;
  can_check_file_in_ils_check_one: boolean;
  can_check_file_in_ils_check_two: boolean;
  can_manage_markups: boolean;
}

interface IsFetchingActionType {
  type: ProjectActionTypes.CHANGE_IS_FETCHING;
  payload: boolean;
}

interface ReceiveProjectsActionTypes {
  type: ProjectActionTypes.RECEIVE_ALL_PROJECTS;
  payload: Array<IProject>;
}

interface AddProjectAtionType {
  type: ProjectActionTypes.ADD_PROJECT;
  payload: IProject;
}

interface RedirectToNewProjectActionType {
  type: ProjectActionTypes.REDIRECT_TO_NEW_PROJECT;
  payload: boolean;
}

interface ReceiveUpdatedProject {
  type: ProjectActionTypes.RECIEVE_UPDATED_PROJECT;
  payload: IUpdatedProject;
}

interface ChangeRemovButtons {
  type: ProjectActionTypes.CHANGE_REMOVE_BUTTONS;
  payload: boolean;
}

interface ResetChackedProjects {
  type: ProjectActionTypes.RESET_CHECKED_PROJECTS;
}

interface AddCheckedProjects {
  type: ProjectActionTypes.ADD_CHECKED_PROJECTS;
  payload: string;
}

interface DeleteProjects {
  type: ProjectActionTypes.DELETE_PROJECTS;
  payload: [];
}

export type ActionType =
  | IsFetchingActionType
  | ReceiveProjectsActionTypes
  | AddProjectAtionType
  | RedirectToNewProjectActionType
  | ReceiveUpdatedProject
  | ChangeRemovButtons
  | ResetChackedProjects
  | AddCheckedProjects
  | DeleteProjects;
