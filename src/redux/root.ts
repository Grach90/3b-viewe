import { combineReducers } from "redux";

import LoginReducer from "./login/LoginReducer";
import SessionReducer from "./sessions/SessionsReducer";
import GlobalReducer from "./global/globalReducer";
import LoggedInReducer from "./loggedIn/LoggedInReducer";
import ProjectReducer from "./projects/ProjectReducer";
import PortalReducer from "./portal/PortalReducer";
import FilesReducer from "./files/FilesReducer";

export const rootReducer = combineReducers({
  loginState: LoginReducer,
  sessionState: SessionReducer,
  globalState: GlobalReducer,
  loggedInState: LoggedInReducer,
  projectState: ProjectReducer,
  portalState: PortalReducer,
  filesState: FilesReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
