export interface IInitState {
  loggedIn: boolean;
}

export enum LoggedInActionTypes {
  CHANGE_LOGGEDIN_TRUE = "CHANGE_LOGGEDIN_TRUE",
  CHANGE_LOGGEDIN_FALSE = "CHANGE_LOGGEDIN_FALSE",
}

interface LoggedInTrueAction {
  type: LoggedInActionTypes.CHANGE_LOGGEDIN_TRUE;
}

interface LoggedInFalseAction {
  type: LoggedInActionTypes.CHANGE_LOGGEDIN_FALSE;
}

export type LoggedInAction = LoggedInTrueAction | LoggedInFalseAction;
