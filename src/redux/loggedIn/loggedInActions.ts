import { LoggedInActionTypes } from "./LoggedInTypes";

export const changeLoggedInTrue = () => ({
  type: LoggedInActionTypes.CHANGE_LOGGEDIN_TRUE,
});
export const changeLoggedInFalse = () => ({
  type: LoggedInActionTypes.CHANGE_LOGGEDIN_FALSE,
});
