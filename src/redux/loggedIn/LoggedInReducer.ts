import {
  IInitState,
  LoggedInAction,
  LoggedInActionTypes,
} from "./LoggedInTypes";

const initState: IInitState = {
  loggedIn: false,
};

const LoggedInReducer = (
  state: IInitState = initState,
  action: LoggedInAction
): IInitState => {
  switch (action.type) {
    case LoggedInActionTypes.CHANGE_LOGGEDIN_TRUE: {
      return {
        loggedIn: true,
      };
    }
    case LoggedInActionTypes.CHANGE_LOGGEDIN_FALSE: {
      return {
        loggedIn: false,
      };
    }
    default:
      return state;
  }
};

export default LoggedInReducer;
