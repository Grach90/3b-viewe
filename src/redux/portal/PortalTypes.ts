export enum PortalActionTypes {
  CHANGE_PORTAL = "CHANGE_PORTAL",
  CHANGE_YES_NO_PORTAL = "CHANGE_YES_NO_PORTAL",
}

export interface IinitState {
  bool_yes_or_no: boolean;
  value_es_or_no: string;
  bool: boolean;
  value: string;
  // function_for_submit(): void;
}

export type PayloadChangePortal = {
  bool: boolean;
  value: string;
};

type PayloadChngeYesNoPortal = {
  bool: boolean;
  value: string;
  // function_for_submit(): void;
};

export interface ChangePortalActionType {
  type: PortalActionTypes.CHANGE_PORTAL;
  payload: PayloadChangePortal;
}

export interface ChngeYesNoPortalActionType {
  type: PortalActionTypes.CHANGE_YES_NO_PORTAL;
  payload: PayloadChngeYesNoPortal;
}

export type ActionTypes = ChangePortalActionType | ChngeYesNoPortalActionType;
