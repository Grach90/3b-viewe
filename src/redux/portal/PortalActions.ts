import { Dispatch } from "redux";
import {
  PortalActionTypes,
  ChangePortalActionType,
  ChngeYesNoPortalActionType,
  ActionTypes,
} from "./PortalTypes";

const ChangePortalAction = (
  bool: boolean,
  value: string
): ChangePortalActionType => ({
  type: PortalActionTypes.CHANGE_PORTAL,
  payload: { bool, value },
});

const ChangeYesNoPortalAction = (
  bool: boolean,
  value: string
): ChngeYesNoPortalActionType => ({
  type: PortalActionTypes.CHANGE_YES_NO_PORTAL,
  payload: { bool, value },
});

export const ChangePortal =
  (bool: boolean, value: string = "") =>
  (dispatch: Dispatch<ActionTypes>) => {
    dispatch(ChangePortalAction(bool, value));
  };
export const ChangeYesNoPortal =
  (bool: boolean, value: string = "") =>
  (dispatch: Dispatch<ActionTypes>) => {
    dispatch(ChangeYesNoPortalAction(bool, value));
  };
