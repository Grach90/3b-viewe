import { IinitState, PortalActionTypes, ActionTypes } from "./PortalTypes";

const initState: IinitState = {
  bool_yes_or_no: false,
  bool: false,
  value: "",
  value_es_or_no: "",
  // function_for_submit: () => {
  //   return;
  // },
};

const PortalReducer = (state = initState, action: ActionTypes): IinitState => {
  switch (action.type) {
    case PortalActionTypes.CHANGE_PORTAL: {
      return {
        ...state,
        bool: action.payload.bool,
        value: action.payload.value,
      };
    }
    case PortalActionTypes.CHANGE_YES_NO_PORTAL: {
      return {
        ...state,
        bool_yes_or_no: action.payload.bool,
        value_es_or_no: action.payload.value,
        // function_for_submit: action.payload.function_for_submit,
      };
    }
    default:
      return state;
  }
};

export default PortalReducer;
