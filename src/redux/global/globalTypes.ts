export enum GlobalActionTypes {
  CHANGE_MODE = "CHANGE_MODE",
  CHANGE_LANGUAGE = "CHANGE_LANGUAGE",
  CHANGE_LOGGEDIN = "CHANGE_LOGGEDIN",
}

export interface IinitState {
  dark_mode: boolean;
  changeLanguage: number;
  loggedIn: boolean;
}

interface ChangeModeType {
  type: GlobalActionTypes.CHANGE_MODE;
  payload: boolean;
}

interface ChangeLenguage {
  type: GlobalActionTypes.CHANGE_LANGUAGE;
}

interface changeLoggedIn {
  type: GlobalActionTypes.CHANGE_LOGGEDIN;
  payload: boolean;
}

export type GlobalAction = ChangeModeType | ChangeLenguage | changeLoggedIn;
