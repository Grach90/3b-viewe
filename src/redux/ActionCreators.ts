import * as GlobalActionCreators from "./global/globalActions";
import * as SessionActionCreators from "./sessions/SessionsActions";
import * as LoginActionCreators from "./login/LoginActions";
import * as ProjectActionCreators from "./projects/ProjectActions";
import * as PortalActionCreators from "./portal/PortalActions";
import * as FilesActionCreators from "./files/FilesActions";

export default {
  ...GlobalActionCreators,
  ...SessionActionCreators,
  ...LoginActionCreators,
  ...ProjectActionCreators,
  ...PortalActionCreators,
  ...FilesActionCreators,
};
