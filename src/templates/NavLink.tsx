import React from "react";
import { INavLink } from "./types";
import { NavLink } from "react-router-dom";

const Navlink: React.FC<INavLink> = ({ children, styles, path, dark_mode }) => {
  return (
    <NavLink
      to={path}
      className={(navDta) => {
        if (navDta.isActive)
          return dark_mode
            ? styles.active + " " + styles.navLink
            : styles.activeLight + " " + styles.navLinkLight;
        else return dark_mode ? styles.navLink : styles.navLinkLight;
      }}
    >
      {children}
    </NavLink>
  );
};

export default Navlink;
