export interface INavLink {
  dark_mode: boolean;
  styles: IStyles;
  path: string;
}

interface IStyles {
  [key: string]: string;
}
