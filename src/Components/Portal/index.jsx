import React, {useEffect} from "react";
import * as ReactDOM from "react-dom";
import {FormattedMessage} from "react-intl";

import messages from "./messages";

import Styles from "./styles.module.css";

const Portal = props => {
  const el = document.createElement("div");
  const portal = document.getElementById("portal-root");

  useEffect(() => {
    portal.appendChild(el);

    return function reset() {
      portal.removeChild(el);
    };
  });

  return ReactDOM.createPortal(props.children, el);
};

const Modal = props => {
  return (
    <Portal>
      <div className={props.dark_mode ? Styles.modal : Styles.modalLight}>
        <div
          className={
            props.dark_mode ? Styles.modalChild : Styles.modalChildLight
          }
        >
          <div className="row h-100 px-3">
            <div className="col h-100">
              <div className="row h-50 d-flex align-items-center">
                <div
                  className={
                    "col d-flex justify-content-center " + Styles.headerFont
                  }
                >
                  {props.portal_value}
                </div>
              </div>

              <div className="row h-50 d-flex align-items-center">
                <div className="col d-flex justify-content-center">
                  <button
                    className={"btn btn-secondary " + Styles.btnFont}
                    onClick={() => props.close_portal()}
                    style={{color: "white"}}
                  >
                    {" "}
                    <FormattedMessage {...messages.closeButton} />
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Portal>
  );
};

export default Modal;
