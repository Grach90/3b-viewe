import React, { useEffect } from "react";
import * as ReactDOM from "react-dom";
import { FormattedMessage } from "react-intl";

import messages from "./messages";

import Styles from "./styles.module.css";

const Portal: React.FC = (props) => {
  const el = document.createElement("div");
  const portal: HTMLElement | null = document.getElementById("portal-root");

  useEffect(() => {
    portal?.appendChild(el);

    return function reset() {
      portal?.removeChild(el);
    };
  });

  return ReactDOM.createPortal(props.children, el);
};

interface IModalProps {
  submit(): void;
  close_portal(): void;
  portal_value: string;
  dark_mode: Boolean;
}

const Modal: React.FC<IModalProps> = (props) => {
  return (
    <Portal>
      <div className={props.dark_mode ? Styles.modal : Styles.modalLight}>
        <div
          className={
            props.dark_mode ? Styles.modalChild : Styles.modalChildLight
          }
        >
          <div className="row h-100 px-3">
            <div className="col h-100">
              <div className="row h-50 d-flex align-items-center">
                <div
                  className={
                    "col d-flex justify-content-center " + Styles.headerFont
                  }
                >
                  {props.portal_value}
                </div>
              </div>

              <div className="row h-50 d-flex align-items-center">
                <div className="col d-flex justify-content-center">
                  <div className="row w-75 justify-content-around">
                    <button
                      className={"btn btn-secondary col-5 " + Styles.btnFont}
                      onClick={() =>
                        // props.project_name_from_admin
                        //   ? props.submit(props.project_name_from_admin)
                        // :
                        props.submit()
                      }
                      style={{ color: "white" }}
                    >
                      <FormattedMessage {...messages.confirmButton} />
                    </button>

                    <button
                      className={"btn btn-secondary col-5 " + Styles.btnFont}
                      onClick={() => props.close_portal()}
                      style={{ color: "white" }}
                    >
                      <FormattedMessage {...messages.denyButton} />
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Portal>
  );
};

export default Modal;
