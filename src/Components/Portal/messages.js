import {defineMessages} from "react-intl";

export default defineMessages({
  closeButton: {
    id: "boilerplate.portals.closeButton",
    defaultMessages: "Close",
  },
  confirmButton: {
    id: "boilerplate.sureToDelete.confirm",
    defaultMessages: "Yes",
  },
  denyButton: {
    id: "boilerplate.sureToDelete.deny",
    defaultMessages: "No",
  }
});
