import { defineMessages } from "react-intl";

export default defineMessages({
  header: {
    id: "boilerplate.HomePage.header",
    defaultMessage: "WERK SNELLER EN EFFECTIEVER MET REVIT",
  },
  contentFirst: {
    id: "boilerplate.HomePage.contentFirst",
    defaultMessage:
      "3B Tools is the Dutch application for Autodesk Revit, with which architects,",
  },
  subtitle: {
    id: "boilerplate.HomePage.subtitle",
    defaultMessage: "3B VIEW IS EEN ONDERDEEL VAN 3B TOOLS",
  },
  contentSecond: {
    id: "boilerplate.HomePage.contentSecond",
    defaultMessage:
      "Contsractors, architectural modellers and constructors work more effectively and faster",
  },
  bottomParagraf: {
    id: "boilerplate.HomePage.bottomParagraf",
    defaultMessage:
      "3B Tools is an in-house developed product of ICN Solutions. We think along with you and offer advice, software, training and systems for the optimization of design and construction processes.",
  },
  phoneInformation: {
    id: "boilerplate.HomePage.phoneInformation",
    defaultMessage:
      "For more information about subscriptions call 073 750 6 750",
  },
  list1: {
    id: "boilerplate.HomePage.content.li_1",
    defaultMessage:
      "3B View is een online platform voor de bouw waarmee u: modellen kunt controleren op de BIM Basis ILS, IFC-bestanden kunt uploaden en inzien, twee revisies van een ontwerp kunt vergelijken, en snapshots en redlining kunt maken van problemen. Voor 3B View is geen softwarelicentie nodig waardoor ook andere partijen, zoals opdrachtgevers, uitgenodigd kunnen worden om deel te nemen aan een project.",
  },
  list2: {
    id: "boilerplate.HomePage.content.li_2",
    defaultMessage:
      "3B Tools is dé Nederlandse applicatie voor Autodesk Revit. Het levert een grote hoeveelheid extra tools en functies waarmee architecten, aannemers, bouwkundig modelleurs en constructeurs sneller en effectiever werken. ",
  },
  list3: {
    id: "boilerplate.HomePage.content.li_3",
    defaultMessage:
      "Hierdoor levert het een veelvoud aan tijd- en kostenbesparing op. 3B Tools ondersteunt onder meer BIM en het Nederlandse Bouwbesluit.",
  },
  watchVideoButton: {
    id: "boilerplate.HomePage.content.watchVideoButton",
    defaultMessage: "Watch Video",
  },
  signIn: {
    id: "boilerplate.Navbar.items.signIn",
    defaultMessage: "signIn 1",
  },
});
