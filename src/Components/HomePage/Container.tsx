import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { FormattedMessage } from "react-intl";
// import { useSelector } from "react-redux";

import messages from "./messages";

import Styles from "./styles.module.css";

import rvt from "../../static/images/document-rvt.png";
import dwg from "../../static/images/document-dwg.png";
import ifc from "../../static/images/document-ifc.png";
import nwd from "../../static/images/document-nwd.png";
import rvtdark from "../../static/images/document-rvt-darkmode.png";
import dwgdark from "../../static/images/document-dwg-darkmode.png";
import ifcdark from "../../static/images/document-ifc-darkmode.png";
import nwddark from "../../static/images/document-nwd-darkmode.png";

// interface Iprops {
//   changeMenuBool: boolean;
//   changeMenu(arg: boolean): void;
// }

const Welcom: React.FC = () => {
  const dark_mode: boolean = false;
  // useSelector((state) => state.dark_mode_reducer.mode);

  return (
    <div className={`${Styles.homeCont} container`}>
      <div className={`${Styles.row} row mt-10`}>
        <div className={`${Styles.leftPartParent} `}>
          <div className={Styles.parent}>
            <div className={Styles.leftFlexContainer}>
              <div className={`${Styles.leftPart} `}>
                <div>
                  <div>
                    <h1
                      className={Styles.header}
                      style={{
                        color: dark_mode ? "white" : "black",
                      }}
                    >
                      <FormattedMessage {...messages.header} />
                    </h1>
                  </div>
                </div>

                <div className="row my-2">
                  <div className="col">
                    <h4
                      className={Styles.text}
                      style={{
                        color: dark_mode ? "#989898" : "black",
                      }}
                    >
                      <FormattedMessage {...messages.list1} />
                    </h4>
                  </div>
                </div>
              </div>
              <div className={`${Styles.leftPart} `}>
                <div>
                  <h1 className={Styles.caption}>
                    <FormattedMessage {...messages.subtitle} />
                  </h1>
                </div>

                <div>
                  <h4
                    className={Styles.text}
                    style={{
                      color: dark_mode ? "#989898" : "black",
                      marginBottom: 0,
                    }}
                  >
                    <FormattedMessage {...messages.list2} />
                  </h4>
                </div>
                <div className={Styles.textWithButton}>
                  <div>
                    <h4
                      className={Styles.text}
                      style={{
                        color: dark_mode ? "#989898" : "black",
                        marginBottom: 0,
                      }}
                    >
                      <FormattedMessage {...messages.list3} />
                    </h4>
                  </div>
                  <div>
                    <a
                      href="https://3btools.nl/"
                      className={`nav-link ${Styles.signInBtn} ${Styles.navLink} d-flex align-items-center justify-content-center btn`}
                      style={{ color: "white" }}
                    >
                      3B TOOLS WEBSITE
                    </a>
                  </div>
                </div>
              </div>
            </div>
            <div style={{ height: "92%", padding: "0 100px" }}>
              <div className={Styles.formatsPng}>
                <div>
                  <img src={dark_mode ? ifcdark : ifc} alt="" />
                  <img src={dark_mode ? rvtdark : rvt} alt="" />
                </div>
                <div>
                  <img src={dark_mode ? dwgdark : dwg} alt="" />
                  <img src={dark_mode ? nwddark : nwd} alt="" />
                </div>
                <p
                  style={{
                    color: dark_mode ? "#989898" : "black",
                  }}
                >
                  Voor meer informatie over <br />
                  abonnementen bel 073 750 6 750
                </p>
              </div>
            </div>
          </div>
        </div>
        <p
          className={Styles.bottomP}
          style={{
            color: dark_mode ? "#989898" : "black",
          }}
        >
          <FormattedMessage {...messages.bottomParagraf} />
        </p>
      </div>
    </div>
  );
};

export default Welcom;
