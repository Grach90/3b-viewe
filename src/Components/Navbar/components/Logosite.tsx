import React from "react";
import { NavLink } from "react-router-dom";
import { LogositeIProps } from "../interface";

import Styles from "../styles.module.css";

import Logo from "static/images/logo-3B-View.png";
import LogoDark from "static/images/logo-3B-View-darkmode.png";

const Logosite: React.FC<LogositeIProps> = (props) => {
  return (
    <div className={Styles.logoContainer}>
      <NavLink
        to="/"
        className={
          "d-flex align-items-center " +
          Styles.logo +
          " " +
          `${
            !props.logout
              ? props.dark_mode
                ? Styles.rightBorder
                : Styles.rightBorderDark
              : ""
          }`
        }
        style={{
          textDecoration: "none",
        }}
      >
        <img
          src={props.dark_mode ? LogoDark : Logo}
          alt="logo"
          className={Styles.logoSize}
        />
      </NavLink>
    </div>
  );
};

export default Logosite;
