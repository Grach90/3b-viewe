import React from "react";
// import { NavLink } from "react-router-dom";
import Navlink from "templates/NavLink";
import { FormattedMessage } from "react-intl";
import { Collapse } from "reactstrap";
import Logosite from "./Logosite";

import { MenuIprops } from "../interface";
import messages from "../messages";
import Styles from "../styles.module.css";

import menuIcon from "../../../static/images/baseline_menu_white_18dp.png";
import menuCloseIcon from "../../../static/images/baseline_close_white_18dp.png";
import menuIconDark from "../../../static/images/baseline_menu_black_18dp.png";
import menuCloseIconDark from "../../../static/images/baseline_close_black_18dp.png";

const Menu: React.FC<MenuIprops> = (props) => {
  return (
    <>
      {!props.toggleMenuButton ? (
        <Logosite dark_mode={props.dark_mode} logout={props.logout} />
      ) : (
        <div style={{ display: "flex", alignItems: "center" }}>
          <button
            onClick={() => props.menuToggle()}
            className="toggleButton"
            style={{ outlineStyle: "none" }}
          >
            <img
              src={
                !props.changeMenuBool
                  ? props.dark_mode
                    ? menuIcon
                    : menuIconDark
                  : props.dark_mode
                  ? menuCloseIcon
                  : menuCloseIconDark
              }
              alt=""
            />
          </button>
          <Logosite dark_mode={props.dark_mode} logout={props.logout} />
        </div>
      )}
      {!props.toggleMenuButton ? (
        <ul className={"navbar-nav mr-auto pl-3 " + Styles.ul}>
          <li className={"nav-item px-2 h-100 " + Styles.navItem}>
            <Navlink
              dark_mode={props.dark_mode}
              styles={Styles}
              path={`/${props.company_name}/projects`}
            >
              <FormattedMessage {...messages.company} />
            </Navlink>
          </li>

          <li className={"nav-item px-2 h-100 " + Styles.navItem}>
            <Navlink
              dark_mode={props.dark_mode}
              styles={Styles}
              path={`/${props.company_name}/ilschecker`}
              // className={`nav-link ${
              //   props.dark_mode ? Styles.navLink : Styles.navLinkLight
              // } h-100 d-flex align-items-center`}
            >
              <FormattedMessage {...messages.ilsCheck} />
            </Navlink>
          </li>
          <li className={"nav-item px-2 h-100 " + Styles.navItem}>
            <Navlink
              dark_mode={props.dark_mode}
              styles={Styles}
              path={`/${props.company_name}/format_change`}
            >
              <FormattedMessage {...messages.formatChange} />
            </Navlink>
          </li>
          <li
            className={"nav-item px-2 h-100 " + Styles.navItem}
            // style={{
            //   display:
            //     props.user.role === "3B Manager" ||
            //     props.user.role === "Project Manager"
            //       ? "block"
            //       : "none",
            // }}
          >
            <Navlink
              dark_mode={props.dark_mode}
              styles={Styles}
              path={"/admin"}
            >
              Dashboard
            </Navlink>
          </li>
        </ul>
      ) : (
        <div className={"navbar-nav mr-auto pl-3"}>
          <Collapse
            isOpen={props.changeMenuBool}
            className={
              props.dark_mode ? Styles.colapseBar : Styles.colapseBarLight
            }
          >
            <ul className={"navbar-nav mr-auto " + Styles.ul}>
              <li className={"nav-item px-2 h-100 " + Styles.navItem}>
                <Navlink
                  dark_mode={props.dark_mode}
                  styles={Styles}
                  path={`/${props.company_name}/projects`}
                  // onClick={() => props.click()}
                >
                  <FormattedMessage {...messages.company} />
                </Navlink>
              </li>

              <li className={"nav-item px-2 h-100 " + Styles.navItem}>
                <Navlink
                  dark_mode={props.dark_mode}
                  styles={Styles}
                  path={`/${props.company_name}/ilschecker`}
                  // onClick={() => props.click()}
                >
                  <FormattedMessage {...messages.ilsCheck} />
                </Navlink>
              </li>

              <li className={"nav-item px-2 h-100 " + Styles.navItem}>
                <Navlink
                  dark_mode={props.dark_mode}
                  styles={Styles}
                  path={`/${props.company_name}/format_change`}
                  // onClick={() => props.click()}
                >
                  <FormattedMessage {...messages.formatChange} />
                </Navlink>
              </li>
              <li
                className={"nav-item px-2 h-100 " + Styles.navItem}
                // style={{
                //   display:
                //     props.user.role === "3B Manager" ||
                //     props.user.role === "Project Manager"
                //       ? "block"
                //       : "none",
                // }}
              >
                <Navlink
                  dark_mode={props.dark_mode}
                  styles={Styles}
                  path={"/admin"}
                  // onClick={() => props.click()}
                >
                  Dashboard
                </Navlink>
              </li>
            </ul>
          </Collapse>
        </div>
      )}
    </>
  );
};

export default Menu;
