import React from "react";
import Styles from "../styles.module.css";
import ControlPanel from "./ControlPanel";
import ControlPanelToggle from "./ControlPanelToggle";
import MenuPanel from "./Menu";
import { ViewerIProps } from "../interface";
import { useTypedSelector } from "hooks/useTypedSelector";

const LoginNavbar: React.FC<ViewerIProps> = (props) => {
  const { company_name } = useTypedSelector(
    (state) => state.sessionState.session
  );
  return (
    <nav
      className={"navbar mx-auto navbar-expand w-100 " + Styles.nav}
      id="navBar"
      style={{ backgroundColor: props.dark_mode ? "black" : "white" }}
    >
      <div className={"container " + Styles.container}>
        <MenuPanel {...props} company_name={company_name} />
        {props.windowSize ? (
          <ControlPanel {...props} />
        ) : (
          <ControlPanelToggle {...props} />
        )}
      </div>
    </nav>
  );
};

export default LoginNavbar;
