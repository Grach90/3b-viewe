import { Route, Routes } from "react-router-dom";
import Upload from "./Contents/upload";
import Version from "./Contents/version";
import Styles from "./styles.module.scss";

import image from "static/images/modelImg.png";

const ILSCheck = () => {
  return (
    <div className={Styles.container}>
      <div className={Styles.image}>
        <img src={image} alt="" />
      </div>
      <div className={Styles.sidebar}>
        <Routes>
          <Route path="choosefile" element={<Upload />} />
          <Route path="Version" element={<Version />} />
        </Routes>
      </div>
    </div>
  );
};

export default ILSCheck;
