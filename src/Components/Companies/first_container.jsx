import React, { useEffect, useState } from "react";
// import { connect } from "react-redux";
import { useNavigate, Navigate } from "react-router-dom";

import { useTypedSelector } from "hooks/useTypedSelector";
import Companies from "./index";
import { useActions } from "hooks/useActions";

// import {
//   fetchProjects,
//   changeDeleteValue,
// } from "redux/projects/projects-actions";
// import { redirect_to_login } from "redux/auth/auth-actions";

// import PropTypes from "prop-types";

const FirstEnter = (props) => {
  const { items: projects } = useTypedSelector((state) => state.projectState);
  const { company_name, company_id } = useTypedSelector(
    (state) => state.sessionState.session
  );
  const { fetchProjects } = useActions();

  console.log("FirstEnter", projects);

  useEffect(() => fetchProjects(company_id), [projects.length]);

  // useEffect(() => {
  //   props.redirect_to_login(false);
  //   return function reset() {
  //     return props.changeDeleteValue(0);
  //   };
  // }, []);

  // const projects = props.projects.filter((x) => !!x.id);
  return (
    <div>
      {projects.length === 0 ? (
        // || props.delete_value === 10
        <Companies projects={projects} />
      ) : (
        // navigate(`/${company_name}/projects/${projects[0].name}`)
        <Navigate to={`/${company_name}/projects/${projects[0].name}`} />
      )}
    </div>
  );
};

// FirstEnter.propTypes = {
//   projects: PropTypes.array,
//   delete_value: PropTypes.number,
//   company_name: PropTypes.string,
//   fetchProjects: PropTypes.func,
//   changeDeleteValue: PropTypes.func,
//   redirect_to_login: PropTypes.func,
// };
// const mapStateToProps = state => {
//   return {
//     projects: state.projects.items,
//     delete_value: state.projects.delete,
//     company_name: state.session.user.company_names[0],
//   };
// };

// export default connect(mapStateToProps, {
//   fetchProjects,
//   changeDeleteValue,
//   redirect_to_login,
// })(FirstEnter);

export default FirstEnter;
