import React from "react";
import { Route, Routes } from "react-router-dom";
import { connect } from "react-redux";
import { FormattedMessage } from "react-intl";

import PropTypes from "prop-types";
import { useTypedSelector } from "hooks/useTypedSelector";

import Files from "./Files";
import messages from "../messages";

import Styles from "./styles.module.css";

import foldIcon from "static/images/icons8-folder-128 (3).png";
import foldIconLight from "static/images/lightEmptyProject.png";

const Contents = (props) => {
  const { items: projects } = useTypedSelector((state) => state.projectState);
  return (
    <div className="h-100" style={{ paddingLeft: "1.5%" }}>
      <div className="w-100 h-100">
        <Routes>
          <Route
            path="/"
            element={
              <div className="row h-100">
                <div className="col">
                  <div
                    className={"w-100 " + Styles.tools}
                    style={{
                      borderBottom: props.dark_mode
                        ? "2px solid black"
                        : "2px solid #B9B9B9",
                    }}
                  />

                  <div
                    className={`row w-100 mt-3 mx-auto ${
                      props.dark_mode ? Styles.body : Styles.bodyLight
                    }`}
                  >
                    <div className="col-4  m-auto">
                      <div className="row">
                        <img
                          src={props.dark_mode ? foldIcon : foldIconLight}
                          alt="folder"
                          className="mx-auto"
                          style={{ width: "86px" }}
                        />
                      </div>

                      <div className="row">
                        <h3
                          className={"mx-auto " + Styles.font}
                          style={{
                            fontSize: "20px",
                            color: props.dark_mode ? "#808080" : "black",
                          }}
                        >
                          <FormattedMessage {...messages.noProject} />
                        </h3>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            }
          />
          {projects
            .filter((x) => !!x.id)
            .map((project) => (
              <Route
                index
                key={project.id}
                path={`${project.name}`}
                element={
                  <Files set_bool_for_content={props.set_bool_for_content} />
                }
              />
            ))}
        </Routes>
      </div>
    </div>
  );
};

// Contents.propTypes = {
//   projects: PropTypes.array,
// };

// const mapStateToProps = state => {
//   return {
//     projects: state.projects.items,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default connect(mapStateToProps, {})(Contents);

export default Contents;
