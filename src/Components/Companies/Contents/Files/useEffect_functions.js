import React from "react";
// import * as R from "ramda";
import info_icon from "static/images/info.svg";

export const sorted_file_lis = (list_files, set_list_files_sorting) => {
  set_list_files_sorting(
    list_files.sort((a, b) => {
      if (a.filename > b.filename) {
        return 1;
      } else if (a.filename < b.filename) {
        return -1;
      } else {
        if (a.version < b.version) {
          return 1;
        } else if (a.version > b.version) {
          return -1;
        }
        return 0;
      }
    })
  );
};

export const get_id = (set_id, list_projects, project_name) => {
  set_id(
    !!list_projects.find((p) => p.name === project_name)
      ? list_projects.find((p) => p.name === project_name).id
      : undefined
  );
};
// export const get_project_id = (set_project_id, list_projects, project_name) => {
//   set_project_id(
//     R.pipe(R.find(R.propEq("name", project_name)), R.prop("id"))(list_projects),
//   );
// };
export const get_searched_files = (
  set_search_files,
  search_value,
  list_files_sorting
) => {
  set_search_files(
    search_value === ""
      ? list_files_sorting
      : list_files_sorting.filter((x) =>
          x.filename.toLowerCase().includes(search_value.toLowerCase())
        )
  );
};

export const get_format_of_file = (set_file_format, search) => {
  const format_from_location =
    search.replace("?", "") !== ""
      ? search
          .replace("?", "")
          .split("&")
          .filter((x) => !!x.match(/type/))
      : [];

  set_file_format(
    format_from_location.length !== 0
      ? format_from_location[0].split("=")[1]
      : ""
  );
};

export const get_style_format = (set_style_of_files, search) => {
  const style_format =
    search.replace("?", "") !== ""
      ? search
          .replace("?", "")
          .split("&")
          .filter((x) => !!x.match(/style/))
      : [];

  set_style_of_files(
    style_format.length !== 0 ? style_format[0].split("=")[1] : ""
  );
};

export const get_error_during_uploading = (
  error_during_uploading,
  setModal,
  set_error_messages
) => {
  error_during_uploading.length !== 0 ? setModal(true) : setModal(false);

  const errors = error_during_uploading.map((x) => (
    <div key={x.file_id}>
      <img
        src={info_icon}
        alt="info_icon"
        style={{ width: "16px", marginRight: "7px" }}
      />
      {x.file_name} {x.message}
    </div>
  ));
  set_error_messages(errors);
};

export const get_socket_progress = (socket, get_progress) => {
  if (socket.on !== undefined) {
    socket.on("progress", (percent) => {
      const progress = JSON.parse(percent);
      get_progress(progress);
    });
  }
};

export const get_files = async (emptyCheckArr, id, fetchFiles) => {
  emptyCheckArr();
  if (id !== undefined) return await fetchFiles(id);
};
