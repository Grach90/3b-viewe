import React, { useState, useEffect } from "react";
// import { connect } from "react-redux";
// import { withRouter } from "react-router-dom";
import { useIntl } from "react-intl";
// import { compose } from "redux";

import { v4 as uuid } from "uuid";

import Toolbar from "./toolbar-view";

// import PropTypes from "prop-types";

// import {
//   deleteFiles,
//   downloadFiles,
//   emptyCheckArr,
//   uploadFile,
//   get_progress,
//   change_search_value,
// } from "redux/files/files-actions";

// import { changePortal, changeYesNoPortal } from "redux/portal/portal-actions";
// import {
//   files_ids,
//   get_svf_path,
//   get_path_for_change,
//   get_project_id,
// } from "./useEffect_functions";

const ToolbarContainer = (props) => {
  // const [, company_name, , project_name] = props.match.url.split("/");
  const [popoverOpen, set_popover_open] = useState(false);
  const [window_width, setWindowWidth] = useState(0);
  const [files_id, set_files_id] = useState([]);
  const [svfPath, set_svfPath] = useState([]);
  const [changePath, set_changePath] = useState("");
  const [project_id, set_project_id] = useState("");

  const intl = useIntl();
  const name = React.createRef();
  const point = React.createRef();

  // useEffect(() => {
  //   files_ids(set_files_id, props.checked_arr);
  //   get_svf_path(set_svfPath, props.checked_arr);
  // }, [props.checked_arr]);

  // useEffect(
  //   () =>
  //     get_path_for_change(
  //       set_changePath,
  //       props.location.search,
  //       props.view_type
  //     ),
  //   [props.location.search, props.view_type]
  // );

  // useEffect(
  //   () => get_project_id(set_project_id, props.projects, project_name),
  //   [props.projects, project_name]
  // );

  // useEffect(() => {
  //   setWindowWidth(window.innerWidth);
  // }, [window.innerWidth]);

  // useEffect(() => {
  //   onSubmitForDropDung(props.acceptedFiles);
  // }, [props.acceptedFiles]);

  const toggle = () => set_popover_open(!popoverOpen);

  const on_change = async (e) => point.current.click();

  // const uploadFileInRedux = async (file) => {
  //   const size = (file.size / 1024000).toFixed(2);

  //   if (size > 2490) {
  //     props.changePortal(
  //       true,
  //       `${file.name} ` +
  //         intl.formatMessage({
  //           id: "boilerplate.heavyFilePortal.header",
  //         })
  //     );

  //     return;
  //   }

  //   const category = file.name.split(".").pop();
  //   const file_id = uuid();

  //   const allowed_extensions = ["rvt", "ifc", "nwc", "iam", "ipt", "idw"];
  //   if (allowed_extensions.includes((x) => x !== category)) {
  //     return props.changePortal(
  //       true,
  //       intl.formatMessage({ id: "boilerplate.wrongFilePortal.header" })
  //     );
  //   }

  //   props.get_progress({
  //     filename: file.name,
  //     size: file.size,
  //     progress: 0,
  //     file_id,
  //     project_id,
  //   });

  //   return await props.uploadFile(company_name, project_id, file_id, file);
  // };

  const on_reset = (e) => {
    e.target.reset();
  };

  // const onSubmit = async (e) => {
  //   e.preventDefault();
  //   const files = await e.target.children[1].files;

  //   if (files === undefined) return;

  //   for (let i = 0; i < files.length; i++) {
  //     uploadFileInRedux(files[i]);
  //   }
  //   on_reset(e);
  // };

  // const onSubmitForDropDung = (files) => {
  //   if (files === undefined || files.length === 0) return;

  //   for (let i = 0; i < files.length; i++) {
  //     uploadFileInRedux(files[i]);
  //   }
  // };

  const downloadSubmit = (e) => {
    props.emptyCheckArr();
    e.preventDefault();
    e.stopPropagation();

    const hash_path = window.btoa(
      props.checked_arr.map(
        (x) =>
          x.trees.find((y) => y.extension === "rvt" || y.extension === "ifc")
            .path
      )[0]
    );

    const { filename, id } = props.checked_arr[0];

    return props.downloadFiles(hash_path, filename, id);
  };

  const deleteSubmit = (e) => {
    const user_id = props.user.id;

    set_popover_open(!popoverOpen);
    props.emptyCheckArr();
    props.changeYesNoPortal(false);

    return props.deleteFiles(user_id, files_id);
  };

  const closePortal = () => props.changePortal(false);

  const change_search = (e) => props.change_search_value(e.target.value);

  return (
    <div>
      <Toolbar
        project_id={project_id}
        svfPath={svfPath}
        downloadSubmit={downloadSubmit}
        deleteSubmit={deleteSubmit}
        // onSubmit={onSubmit}
        on_change={on_change}
        point={point}
        name={name}
        // company_name={company_name}
        // project_name={project_name}
        checked_arr={props.checked_arr}
        list_files={props.list_files}
        changeViewType={props.changeViewType}
        view_type={props.view_type}
        popoverOpen={popoverOpen}
        toggle={toggle}
        changePath={changePath}
        location={props.location}
        portal_bool={props.portal_bool}
        portal_value={props.portal_value}
        closePortal={closePortal}
        dark_mode={props.dark_mode}
        change_search={change_search}
        search_value={props.search_value}
        changeYesNoPortal={props.changeYesNoPortal}
        portal_bool_yes_no={props.portal_bool_yes_no}
        portal_value_yes_no={props.portal_value_yes_no}
        function_for_submit={props.function_for_submit}
        set_bool_for_content={props.set_bool_for_content}
        window_width={window_width}
        intl={intl}
        user={props.user}
        permissions={props.permissions}
      />
    </div>
  );
};

// ToolbarContainer.propTypes = {
//   changeValue: PropTypes.number,
//   projects: PropTypes.array,
//   checked_arr: PropTypes.array,
//   search_value: PropTypes.string,
//   portal_value: PropTypes.string,
//   portal_value_yes_no: PropTypes.string,
//   view_type: PropTypes.string,
//   user: PropTypes.object,
//   portal_bool: PropTypes.bool,
//   portal_portal_bool_yes_nobool: PropTypes.bool,
//   function_for_submit: PropTypes.func,
//   downloadFiles: PropTypes.func,
//   deleteFiles: PropTypes.func,
//   emptyCheckArr: PropTypes.func,
//   uploadFile: PropTypes.func,
//   get_progress: PropTypes.func,
//   changePortal: PropTypes.func,
//   change_search_value: PropTypes.func,
//   changeYesNoPortal: PropTypes.func,
// };

// const mapStateToProps = (state) => {
//   return {
//     changeValue: state.files.changeValue,
//     projects: state.projects.items,
//     user: state.session.user,
//     search_value: state.files.search,
//     portal_bool: state.portal.bool,
//     portal_value: state.portal.value,
//     portal_bool_yes_no: state.portal.bool_yes_or_no,
//     portal_value_yes_no: state.portal.value_es_or_no,
//     function_for_submit: state.portal.function_for_submit,
//     checked_arr: state.files.checkedArr,
//     view_type: state.files.viewType,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default compose(
//   connect(mapStateToProps, {
//     downloadFiles,
//     deleteFiles,
//     emptyCheckArr,
//     uploadFile,
//     get_progress,
//     changePortal,
//     change_search_value,
//     changeYesNoPortal,
//   }),
//   withRouter
// )(ToolbarContainer);

export default ToolbarContainer;
