import React from "react";
import {useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {useIntl} from "react-intl";

import Styles from "../styles.module.css";

import convert_white from "static/images/convert_white.png";
import convert_black from "static/images/convert_black.png";

export const FormatChange = props => {
  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);
  const intl = useIntl();
  return (
    <div
      style={{
        float: "right",
        margin: window.innerWidth >= 1024 ? "16px 3px" : "16px 6px",
        display: "flex",
      }}
    >
      <NavLink
        to={`/${props.user.company_names[0]}/format_change/${props.project_id}`}
        className={dark_mode ? Styles.format : Styles.formatLight}
        title={intl.formatMessage({
          id: "boilerplate.CompanyPage.formatChange.title",
        })}
      >
        <img src={dark_mode ? convert_white : convert_black} />
      </NavLink>
    </div>
  );
};
