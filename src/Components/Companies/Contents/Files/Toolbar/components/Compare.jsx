import React from "react";
import {useIntl} from "react-intl";
import {useSelector} from "react-redux";

import Styles from "../styles.module.css";
import StylesDtop from "./Toolsdrop/styles.module.css";

import compare from "static/images/compare.png";
import compareActive from "static/images/compareActive.png";
import compareActiveLight from "static/images/CompareLight.png";
import {DropdownItem} from "reactstrap";

export const Compare = props => {
  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);
  const intl = useIntl();

  let version_1;
  let version_2;

  if (props.checked_arr.length === 2) {
    version_1 = props.checked_arr[0].version;
    version_2 = props.checked_arr[1].version;
  }

  return window.innerWidth > 1024 ? (
    <div
      style={{
        width: "40px",
        display:
          props.checked_arr.length === 2 || props.checked_arr.length === 0
            ? props.permission === true
              ? "block"
              : "none"
            : "none",
      }}
    >
      <a
        href={`/compare.html?${props.svfPath[0]}?${version_1}?${props.svfPath[1]}?${version_2}`}
        style={{
          pointerEvents: props.checked_arr.length === 2 ? "auto" : "none",
        }}
      >
        <button
          className={`d-flex justify-content-center align-items-center ${
            dark_mode ? Styles.toolBtn : Styles.toolBtnLight
          }`}
          disabled={props.checked_arr.length === 2 ? false : true}
          title={intl.formatMessage({
            id: "boilerplate.CompanyPage.compareButton.title",
          })}
          style={{
            boxShadow:
              dark_mode && props.checked_arr.length === 2
                ? "0px 0px 7px -3px"
                : "none",
          }}
        >
          <img
            src={
              props.checked_arr.length === 2
                ? dark_mode
                  ? compareActive
                  : compareActiveLight
                : compare
            }
            alt="icon"
          />
        </button>
      </a>
    </div>
  ) : props.checked_arr.length === 2 || props.checked_arr.length === 0 ? (
    <DropdownItem
      className={
        dark_mode
          ? StylesDtop.dropdownItem
          : StylesDtop.dropdownItemLight +
            " justify-content-center align-items-center"
      }
      style={{
        border: "none",
        width: "159px",
        color: dark_mode ? "#BFBFBF" : "black",
        display: props.permission === true ? "flex" : "none",
      }}
    >
      <a
        href={`/compare.html?${props.svfPath[0]}?${version_1}?${props.svfPath[1]}?${version_2}`}
        style={{
          color: dark_mode ? "#BFBFBF" : "black",
          pointerEvents: props.checked_arr.length === 2 ? "auto" : "none",
        }}
      >
        Compare
      </a>
    </DropdownItem>
  ) : null;
};
