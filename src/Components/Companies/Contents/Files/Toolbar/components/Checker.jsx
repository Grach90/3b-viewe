import React from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

import Styles from "../styles.module.css";
import StylesDtop from "./Toolsdrop/styles.module.css";

import ifc from "static/images/ifc.png";
import ifcActive from "static/images/ifcActive.png";
import ifcActiveLight from "static/images/ILScheckLight.png";
import {DropdownItem} from "reactstrap";

export const Checker = props => {
  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);
  return window.innerWidth > 1024 ? (
    <div
      style={{
        width: "40px",
        display:
          (props.checked_arr.length === 1 &&
            !!props.checked_arr[0].trees.find(x => x.extension === "ifc")) ||
          props.checked_arr.length === 0
            ? props.permission === true
              ? "block"
              : "none"
            : "none",
      }}
    >
      <Link
        to={
          props.checked_arr.length === 1
            ? `/${props.company_name}/ilschecker/${props.project_name}/${props.project_id}/${props.checked_arr[0].id}`
            : "/"
        }
        style={{
          pointerEvents: props.checked_arr.length === 1 ? "auto" : "none",
        }}
      >
        <button
          className={`d-flex justify-content-center align-items-center ${
            dark_mode ? Styles.toolBtn : Styles.toolBtnLight
          }`}
          title="Check IFC File"
          disabled={props.checked_arr.length === 1 ? false : true}
          style={{
            boxShadow:
              !dark_mode && props.checked_arr.length === 1
                ? "0px 0px 7px -3px"
                : "none",
          }}
        >
          <img
            src={
              props.checked_arr.length === 1
                ? dark_mode
                  ? ifcActive
                  : ifcActiveLight
                : ifc
            }
            alt="icon"
            style={{
              width: "16px",
              height: "19px",
            }}
          />
        </button>
      </Link>
    </div>
  ) : (props.checked_arr.length === 1 &&
      !!props.checked_arr[0].trees.find(x => x.extension === "ifc")) ||
    props.checked_arr.length === 0 ? (
    <DropdownItem
      className={
        dark_mode
          ? StylesDtop.dropdownItem
          : StylesDtop.dropdownItemLight +
            " justify-content-center align-items-center"
      }
      style={{
        border: "none",
        width: "159px",
        color: dark_mode ? "#BFBFBF" : "black",
        display: props.permission === true ? "flex" : "none",
      }}
    >
      <Link
        style={{color: dark_mode ? "#BFBFBF" : "black"}}
        to={
          props.checked_arr.length === 1
            ? `/${props.company_name}/ilschecker/${props.project_name}/${props.project_id}/${props.checked_arr[0].id}`
            : "/"
        }
      >
        Checker
      </Link>
    </DropdownItem>
  ) : null;
};
