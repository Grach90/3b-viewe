import React from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

import Styles from "../styles.module.css";

import ifc from "static/images/ils_check_2.png";
import ifcActive from "static/images/ils_check_2_active.png";
import ifcActiveLight from "static/images/ils_ckeck_2_light_active.png";
import {DropdownItem} from "reactstrap";
import StylesDtop from "./Toolsdrop/styles.module.css";

export const CheckerTwo = props => {
  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);
  return window.innerWidth > 1024 ? (
    <div
      style={{
        width: window.innerWidth > 750 ? "40px" : "30px",
        display:
          (props.checked_arr.length === 1 &&
            !!props.checked_arr[0].trees.find(x => x.extension === "ifc")) ||
          props.checked_arr.length === 0
            ? props.permission === true
              ? "block"
              : "none"
            : "none",
      }}
    >
      <Link
        to={
          props.checked_arr.length === 1
            ? `/${props.company_name}/ilscheckerTwo/${props.project_name}/${props.project_id}/${props.checked_arr[0].id}`
            : "/"
        }
        style={{
          pointerEvents: props.checked_arr.length === 1 ? "auto" : "none",
        }}
      >
        <button
          className={`d-flex  ${
            dark_mode ? Styles.toolBtn : Styles.toolBtnLight
          }`}
          title="Check IFC File"
          disabled={props.checked_arr.length === 1 ? false : true}
          style={{
            boxShadow:
              !dark_mode && props.checked_arr.length === 1
                ? "0px 0px 7px -3px"
                : "none",
          }}
        >
          <img
            src={
              props.checked_arr.length === 1
                ? dark_mode
                  ? ifcActive
                  : ifcActiveLight
                : ifc
            }
            alt="icon"
            style={{
              width: "20px",
              height: "24px",
              margin: "4px 0 0 3px",
            }}
          />
        </button>
      </Link>
    </div>
  ) : (props.checked_arr.length === 1 &&
      !!props.checked_arr[0].trees.find(x => x.extension === "ifc")) ||
    props.checked_arr.length === 0 ? (
    <DropdownItem
      className={
        dark_mode
          ? StylesDtop.dropdownItem
          : StylesDtop.dropdownItemLight +
            " justify-content-center align-items-center"
      }
      style={{
        border: "none",
        width: "159px",
        color: dark_mode ? "#BFBFBF" : "black",
        display: props.permission ? "flex" : "none",
      }}
    >
      <Link
        style={{color: dark_mode ? "#BFBFBF" : "black"}}
        to={
          props.checked_arr.length === 1
            ? `/${props.company_name}/ilschecker/${props.project_name}/${props.project_id}/${props.checked_arr[0].id}`
            : "/"
        }
      >
        Checker Two
      </Link>
    </DropdownItem>
  ) : null;
};
