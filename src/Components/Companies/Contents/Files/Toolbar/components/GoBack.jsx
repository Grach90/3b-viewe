import React from "react";
import Styles from "../styles.module.css";
import backIcon from "static/images/backIcon.png";
import {useSelector} from "react-redux";

export const GoBack = props => {
  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);
  return (
    <div style={{width: "40px"}}>
      <form>
        <button
          type="submit"
          className={`d-flex justify-content-center align-items-center ${
            dark_mode ? Styles.toolBtn : Styles.toolBtnLight
          }`}
        >
          <img src={backIcon} alt="icon" />
        </button>
      </form>
    </div>
  );
};
