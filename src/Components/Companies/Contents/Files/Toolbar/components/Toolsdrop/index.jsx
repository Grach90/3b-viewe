import React, { useState } from "react";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

import Styles from "./styles.module.css";
import "./drop.css";

import toolsListIcon from "static/images/toolsListIcon.png";
import toolsListIconDark from "static/images/toolsListIconDark.png";
import toolsListIconDisabled from "static/images/toolsListIconDisabled.png";

import * as Tools from "./../index";

const DropTools = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggle = () => setDropdownOpen((prevState) => !prevState);
  return (
    <div className={"h-100 d-flex justify-content-end " + Styles.font}>
      <Dropdown
        isOpen={dropdownOpen}
        toggle={toggle}
        // disabled={props.checked_arr.length !== 0 ? false : true}
        style={{ width: "30px", height: "30px", marginRight: "2px" }}
      >
        <DropdownToggle
          caret
          className={"dropdownClass " + Styles.dropdownClass}
          style={
            window.innerWidth > 410
              ? {
                  backgroundColor: props.dark_mode ? "#111111" : "white",
                  color: props.dark_mode ? "#BFBFBF" : "black",
                  border: "none",
                  width: "35px",
                  height: "35px",
                  padding: "0px",
                }
              : {
                  backgroundColor: props.dark_mode ? "#111111" : "white",
                  border: "none",
                  width: "30px",
                  height: "30px",
                  padding: "0px",
                }
          }
        >
          <img
            src={
              // props.checked_arr.length !== 0
              //   ?
              props.dark_mode ? toolsListIconDark : toolsListIcon
              // : toolsListIconDisabled
            }
            alt="drop"
            width="23px"
          />
        </DropdownToggle>
        <DropdownMenu
          className={props.dark_mode ? Styles.dropMenu : Styles.dropMenuLight}
        >
          <Tools.download
            checked_arr={props.checked_arr}
            downloadSubmit={props.downloadSubmit}
            dark_mode={props.dark_mode}
          />
          <Tools.viewer
            checked_arr={props.checked_arr}
            svfPath={props.svfPath}
            company_name={props.company_name}
            project_name={props.project_name}
            dark_mode={props.dark_mode}
          />
          <Tools.compare
            className={
              props.dark_mode ? Styles.dropdownItem : Styles.dropdownItemLight
            }
            checked_arr={props.checked_arr}
            svfPath={props.svfPath}
            dark_mode={props.dark_mode}
          />
          <Tools.checker
            checked_arr={props.checked_arr}
            company_name={props.company_name}
            project_name={props.project_name}
            project_id={props.project_id}
            dark_mode={props.dark_mode}
          />
          <Tools.checker_two
            checked_arr={props.checked_arr}
            company_name={props.company_name}
            project_name={props.project_name}
            project_id={props.project_id}
            dark_mode={props.dark_mode}
          />
          <Tools.drop
            checked_arr={props.checked_arr}
            deleteSubmit={props.deleteSubmit}
            popoverOpen={props.popoverOpen}
            toggle={props.toggle}
            dark_mode={props.dark_mode}
            changeYesNoPortal={props.changeYesNoPortal}
            portal_bool_yes_no={props.portal_bool_yes_no}
            portal_value_yes_no={props.portal_value_yes_no}
            function_for_submit={props.function_for_submit}
          />
        </DropdownMenu>
      </Dropdown>
    </div>
  );
};

export default DropTools;
