import React, { useState, useEffect } from "react";
// import { connect } from "react-redux";
// import { withRouter } from "react-router-dom";

// import {compose} from "ramda";

import Toolsdrop from "./index";

// import { changeFormat } from "redux/files/files-actions";

const ToolsdropContainer = (props) => {
  // const [, company_name, , project_name] = props.match.url.split("/");

  const [bool, setBool] = useState(false);

  // const f_file = props.location.search.replace("?", "");
  // const seachPath = props.location.search.replace("?", "");
  // const types = seachPath !== "" ? seachPath.split("&&") : [];

  // const getType =
  //   types.length !== 0 ? types.filter((x) => !!x.match(/type/)) : [];

  // const changePath =
  //   getType.length === 0
  //     ? types.length === 0
  //       ? props.format_file
  //       : seachPath + "&&" + props.format_file
  //     : types
  //         .map((x) => {
  //           if (!!x.match(/type/)) {
  //             return `${props.format_file}`;
  //           }

  //           return x;
  //         })
  //         .join("&&");

  // const getPath =
  //   props.format_file === "" ? changePath.replace("&&", "") : changePath;

  // useEffect(() => setBool(false), [bool]);

  // const onChange = (format) => {
  //   setBool(true);

  //   if (format === "ALL") return props.changeFormat("");

  //   const path = `type=${format}`;

  //   return props.changeFormat(path);
  // };

  return (
    <Toolsdrop
      format_file={props.format_file}
      // c_name={company_name}
      // project_name={project_name}
      // f_file={f_file}
      bool={bool}
      // changeFormat={changeFormat}
      // onChange={onChange}
      // getPath={getPath}
      dark_mode={props.dark_mode}
      checked_arr={props.checked_arr}
      downloadSubmit={props.downloadSubmit}
      svfPath={props.svfPath}
      company_name={props.company_name}
      project_id={props.project_id}
      deleteSubmit={props.deleteSubmit}
      popoverOpen={props.popoverOpen}
      toggle={props.toggle}
      changeYesNoPortal={props.changeYesNoPortal}
      portal_bool_yes_no={props.portal_bool_yes_no}
      portal_value_yes_no={props.portal_value_yes_no}
      function_for_submit={props.function_for_submit}
    />
  );
};

// const mapStateToProps = state => {
//   return {
//     format_file: state.files.format_file,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default compose(
//   connect(mapStateToProps, {changeFormat}),
//   withRouter,
// )(ToolsdropContainer);

export default ToolsdropContainer;
