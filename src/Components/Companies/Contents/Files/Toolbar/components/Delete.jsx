import React from "react";
import { useSelector } from "react-redux";

import Modal from "Components/Portal/yes_no_portal";

import Styles from "../styles.module.css";
import StylesDtop from "./Toolsdrop/styles.module.css";
import "./addPopover/addPopover.css";

import delBtn from "static/images/del.png";
import delBtnActive from "static/images/delActive.png";
import delBtnActiveLight from "static/images/DeleteLight.png";

import { DropdownItem } from "reactstrap";

export const Delete = (props) => {
  const closePortal = () => props.changeYesNoPortal(false);
  const dark_mode = useSelector((state) => state.dark_mode_reducer.mode);

  const changePortal = () =>
    props.changeYesNoPortal(
      true,
      " Are you sure you want to delete?",
      props.deleteSubmit
    );

  return window.innerWidth > 1024 ? (
    <div style={{ width: "40px" }}>
      <button
        type="button"
        className={`d-flex justify-content-center align-items-center ${
          dark_mode ? Styles.toolBtn : Styles.toolBtnLight
        }`}
        disabled={props.checked_arr.length !== 0 ? false : true}
        style={{
          pointerEvents: props.checked_arr.length !== 0 ? "auto" : "none",
          boxShadow:
            !dark_mode && props.checked_arr.length !== 0
              ? "0px 0px 7px -3px"
              : "none",
        }}
        onClick={() => changePortal()}
      >
        <img
          src={
            props.checked_arr.length !== 0
              ? dark_mode
                ? delBtnActive
                : delBtnActiveLight
              : delBtn
          }
          alt="icon"
          style={{ width: "14px" }}
        />
      </button>

      {props.portal_bool_yes_no && (
        <Modal
          portal_value={props.portal_value_yes_no}
          close_portal={closePortal}
          dark_mode={dark_mode}
          submit={props.function_for_submit}
        />
      )}
    </div>
  ) : (
    <div>
      <DropdownItem
        type="button"
        className={
          dark_mode
            ? StylesDtop.dropdownItem
            : StylesDtop.dropdownItemLight +
              " d-flex justify-content-center align-items-center"
        }
        onClick={() => changePortal()}
        style={{
          color: dark_mode ? "#BFBFBF" : "black",
          width: "159px",
        }}
      >
        Delete
      </DropdownItem>
      {props.portal_bool_yes_no && (
        <Modal
          portal_value={props.portal_value_yes_no}
          close_portal={closePortal}
          dark_mode={dark_mode}
          submit={props.function_for_submit}
        />
      )}
      {props.portal_bool_yes_no && (
        <Modal
          portal_value={props.portal_value_yes_no}
          close_portal={closePortal}
          dark_mode={dark_mode}
          submit={props.function_for_submit}
        />
      )}
    </div>
  );
};
