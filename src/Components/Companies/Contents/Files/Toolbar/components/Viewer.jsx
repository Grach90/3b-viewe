import React from "react";
import { useIntl } from "react-intl";
import { useSelector } from "react-redux";

import Styles from "../styles.module.css";
import StylesDtop from "./Toolsdrop/styles.module.css";

import viewer from "static/images/viewer.png";
import viewerLight from "static/images/viewerIconLight.png";
import viewerDis from "static/images/viewerDis.png";
import { DropdownItem } from "reactstrap";
import { useTypedSelector } from "hooks/useTypedSelector";

export const Viewer = (props) => {
  const dark_mode = useTypedSelector((state) => state.globalState);
  // const permission = useSelector(
  //   (state) => state.projects.user_permissions.watch
  // );
  // console.log("props.checked_arr.length", props.checked_arr.length);
  const onclick = () => {
    if (props.svfPath[0] === "") {
      alert('Please wait file is converiting to "SVF"');
    }
  };
  const intl = useIntl();

  return window.innerWidth > 1024 ? (
    <div
      style={{
        width: "40px",
        // display:
        //   props.checked_arr.length <= 2 || props.checked_arr.length === 0
        //     ? permission === true
        //       ? "block"
        //       : "none"
        //     : "none",
      }}
    >
      {props.checked_arr.length === 1 ? (
        <a
          href={
            props.svfPath[0] !== ""
              ? `/viewer.html?${props.company_name}&${props.project_name}&${props.checked_arr[0].id}&view=1`
              : "#"
          }
          style={{
            pointerEvents:
              props.checked_arr.length <= 2 && props.checked_arr.length !== 0
                ? "auto"
                : "none",
          }}
        >
          <button
            className={`justify-content-center align-items-center ${
              dark_mode ? Styles.toolBtn : Styles.toolBtnLight
            }`}
            title={intl.formatMessage({
              id: "boilerplate.CompanyPage.viewerButton.title",
            })}
            disabled={
              props.checked_arr.length !== 0 && props.checked_arr.length <= 2
                ? false
                : true
            }
            onClick={() => onclick()}
            style={{
              boxShadow:
                !dark_mode &&
                props.checked_arr.length <= 2 &&
                props.checked_arr.length !== 0
                  ? "0px 0px 7px -3px"
                  : "none",
            }}
          >
            <img
              src={
                props.checked_arr.length <= 2 && props.checked_arr.length !== 0
                  ? dark_mode
                    ? viewer
                    : viewerLight
                  : viewerDis
              }
              alt="icon"
            />
          </button>
        </a>
      ) : (
        <a
          href={
            props.svfPath[0] !== ""
              ? `/viewer.html?${props.company_name}&${props.project_name}&${props.checked_arr[0]?.id}&view=1&${props.checked_arr[1]?.id}&view=1`
              : "#"
          }
          style={{
            pointerEvents:
              props.checked_arr.length !== 0 && props.checked_arr.length <= 2
                ? "auto"
                : "none",
          }}
        >
          <button
            className={`d-flex justify-content-center align-items-center ${
              dark_mode ? Styles.toolBtn : Styles.toolBtnLight
            }`}
            title={intl.formatMessage({
              id: "boilerplate.CompanyPage.viewerButton.title",
            })}
            disabled={
              props.checked_arr.length !== 0 && props.checked_arr.length === 2
                ? false
                : true
            }
            onClick={() => onclick()}
            style={{
              boxShadow:
                !dark_mode && props.checked_arr.length === 2
                  ? "0px 0px 7px -3px"
                  : "none",
            }}
          >
            <img
              src={
                props.checked_arr.length <= 2 && props.checked_arr.length !== 0
                  ? dark_mode
                    ? viewer
                    : viewerLight
                  : viewerDis
              }
              alt="icon"
            />
          </button>
        </a>
      )}
    </div>
  ) : props.checked_arr.length <= 2 ? (
    <DropdownItem
      className={
        dark_mode
          ? StylesDtop.dropdownItem
          : StylesDtop.dropdownItemLight +
            " justify-content-center align-items-center"
      }
      style={{
        border: "none",
        width: "159px",
        color: dark_mode ? "#BFBFBF" : "black",
        // display: permission === true ? "block" : "none",
      }}
      onClick={() => onclick()}
    >
      {props.checked_arr.length === 1 && (
        <a
          href={
            props.svfPath[0] !== ""
              ? `/viewer.html?${props.company_name}&${props.project_name}&${props.checked_arr[0].id}&view=1`
              : "#"
          }
          style={{ color: dark_mode ? "#BFBFBF" : "black" }}
        >
          Viewer
        </a>
      )}
      {props.checked_arr.length === 2 && (
        <a
          href={
            props.svfPath[0] !== ""
              ? `/viewer.html?${props.company_name}&${props.project_name}&${props.checked_arr[0]?.id}&view=1&${props.checked_arr[1]?.id}&view=1`
              : "#"
          }
          style={{ color: dark_mode ? "#BFBFBF" : "black" }}
        >
          Viewer
        </a>
      )}
    </DropdownItem>
  ) : null;
};
