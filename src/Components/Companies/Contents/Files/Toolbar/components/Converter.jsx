import React from "react";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";

import Styles from "../styles.module.css";

import change from "static/images/change.png";
import changeActive from "static/images/changeActive.png";

export const Converter = props => {
  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);
  return (
    <div
      style={{
        width: window.innerWidth > 750 ? "46px" : "30px",
        display:
          props.checked_arr.length === 1 || props.checked_arr.length === 0
            ? "block"
            : "none",
      }}
    >
      <Link
        to={"/format_change"}
        style={{
          pointerEvents: props.checked_arr.length === 1 ? "auto" : "none",
        }}
      >
        <button
          className={`d-flex justify-content-center align-items-center ${
            dark_mode ? Styles.toolBtn : Styles.toolBtnLight
          }`}
          title="Cahnge Format"
          disabled={props.checked_arr.length === 1 ? false : true}
          style={{
            boxShadow:
              !dark_mode && props.checked_arr.length === 1
                ? "0px 0px 7px -3px"
                : "none",
          }}
        >
          <img
            src={props.checked_arr.length === 1 ? changeActive : change}
            alt="icon"
          />
        </button>
      </Link>
    </div>
  );
};
