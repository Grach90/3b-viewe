import React from "react";
import {FormattedMessage, useIntl} from "react-intl";

import Styles from "../styles.module.css";
import messages from "../../../../messages";

import upload from "static/images/upload.png";

export const Upload = props => {
  const intl = useIntl();
  return (
    <div
      style={
        window.innerWidth > 1024
          ? {width: "100%", maxWidth: "138px"}
          : window.innerWidth > 410
          ? {width: "35px"}
          : {width: "30px"}
      }
    >
      <form
        onSubmit={props.onSubmit}
        encType="multipart/form-data"
        className={Styles.form}
      >
        <label
          htmlFor={Styles.file}
          className={
            props.permission === true
              ? Styles.uploadNoFile
              : Styles.uploadNoFileDisabled
          }
          title={intl.formatMessage({
            id:
              props.permission === true
                ? "boilerplate.CompanyPage.uploadButtonTitle"
                : "boilerplate.CompanyPage.uploadButtonTitle.noPermission",
          })}
        >
          <span>
            {window.innerWidth <= 1024 ? (
              <img
                src={upload}
                style={
                  window.innerWidth <= 410
                    ? {paddingBottom: "12%", marginRight: "0px"}
                    : {marginRight: "0px"}
                }
              />
            ) : (
              <FormattedMessage {...messages.uploadButton} />
            )}
          </span>
        </label>

        <input
          type="file"
          id={Styles.file}
          multiple
          ref={props.name}
          onChange={props.on_change}
          // disabled={!props.permission}
        />

        <label htmlFor="sub_btn" />

        <input
          type="submit"
          id="sub_btn"
          ref={props.point}
          style={{display: "none"}}
        />
      </form>
    </div>
  );
};
