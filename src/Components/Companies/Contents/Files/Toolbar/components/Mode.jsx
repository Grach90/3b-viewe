import React, { useEffect, useState } from "react";
import { useIntl } from "react-intl";
// import {Redirect} from "react-router-dom";
import { useSelector } from "react-redux";

import Styles from "../styles.module.css";

import cupStyle from "static/images/cupStyle.png";
import cupStyleActiveLight from "static/images/grid_style_light.png";
import inlineStyle from "static/images/inlineStyle.png";
import inlineStyleLight from "static/images/inline_style_light.png";

export const Mode = (props) => {
  const [bool, setBool] = useState(false);
  const dark_mode = useSelector((state) => state.dark_mode_reducer.mode);

  useEffect(() => {
    return () => {
      return setBool(false);
    };
  }, []);

  const changeType = (type) => {
    setBool(true);
    const path = `style=${type}`;
    props.changeViewType(path);
  };

  const intl = useIntl();
  const getShearch = props.location.search.replace("?", "");
  const getSearchArr = getShearch !== "" ? getShearch.split("&") : [];

  const getType =
    getSearchArr.length !== 0
      ? getSearchArr.filter((x) => !!x.match(/style/))[0]
      : "";

  return (
    <div
      style={{
        float: "right",
        margin: window.innerWidth >= 1024 ? "16px 3px" : "16px 6px",
        display: "flex",
      }}
    >
      {/* {bool && (
        <Redirect
          to={`/${props.company_name}/projects/${props.project_name}?${props.changePath}`}
        />
      )} */}
      <button
        className={`d-flex justify-content-center align-items-center ${
          dark_mode ? Styles.menu : Styles.menuLight
        }`}
        onClick={() => {
          getType === "style=grid" ? changeType("inline") : changeType("grid");
        }}
        style={{ outlineStyle: "none" }}
        title={intl.formatMessage({
          id: "boilerplate.CompanyPage.gridTableButton.title",
        })}
      >
        <img
          src={
            getType === "style=grid"
              ? dark_mode
                ? inlineStyle
                : inlineStyleLight
              : dark_mode
              ? cupStyle
              : cupStyleActiveLight
          }
          alt="icon"
        />
      </button>
    </div>
  );
};
