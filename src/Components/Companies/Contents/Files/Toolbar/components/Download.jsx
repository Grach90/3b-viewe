import React from "react";
import { useSelector } from "react-redux";
import { useIntl } from "react-intl";

import Styles from "../styles.module.css";
import StylesDtop from "./Toolsdrop/styles.module.css";

import save from "static/images/save.png";
import saveActive from "static/images/saveActive.png";
import saveActiveLight from "static/images/downloadIconLight.png";
import saveLight from "static/images/downloadIconLightNoActive.png";
import { DropdownItem } from "reactstrap";
import { useTypedSelector } from "hooks/useTypedSelector";

export const Download = (props) => {
  const intl = useIntl();
  const dark_mode = useTypedSelector((state) => state.globalState);

  return window.innerWidth > 1024 ? (
    <div
      style={{
        width: "40px",
        // display:
        //   props.checked_arr.length === 1 || props.checked_arr.length === 0
        //     ? "block"
        //     : "none",
      }}
    >
      <form>
        <button
          type="submit"
          className={`d-flex justify-content-center align-items-center ${
            dark_mode ? Styles.toolBtn : Styles.toolBtnLight
          }`}
          onClick={props.downloadSubmit}
          // disabled={props.checked_arr.length !== 0 ? false : true}
          style={
            {
              // pointerEvents: props.checked_arr.length !== 0 ? "auto" : "none",
              // boxShadow:
              //   !dark_mode && props.checked_arr.length !== 0
              //     ? "0px 0px 7px -3px"
              //     : "none",
            }
          }
          title={intl.formatMessage({
            id: "boilerplate.CompanyPage.downloadButton.title",
          })}
        >
          <img
            src={
              // props.checked_arr.length !== 0
              //   ? dark_mode
              //     ? saveActive
              //     : saveActiveLight
              //   : dark_mode
              //   ? save
              // :
              saveLight
            }
            alt="icon"
          />
        </button>
      </form>
    </div>
  ) : (
    //  props.checked_arr.length === 1 || props.checked_arr.length === 0 ?
    <DropdownItem
      className={
        dark_mode
          ? StylesDtop.dropdownItem
          : StylesDtop.dropdownItemLight +
            " d-flex justify-content-center align-items-center"
      }
      style={{
        border: "none",
        width: "159px",
        color: dark_mode ? "#BFBFBF" : "black",
      }}
      type="submit"
      onClick={(e) => {
        props.downloadSubmit(e);
      }}
    >
      Download
    </DropdownItem>
  );
  // : null;
};
