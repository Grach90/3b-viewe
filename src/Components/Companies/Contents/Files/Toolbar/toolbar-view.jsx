import React, { useState, useEffect } from "react";

import FormatdropContainer from "./Formatdrop/container";
import Modal from "Components/Portal";
import * as Tools from "./components/index";

import Styles from "./styles.module.css";

import backIcon from "static/images/backIcon.png";
import backIconDark from "static/images/backIconDark.png";

const Toolbar = (props) => {
  if (props.list_files) {
    return (
      <div className="col h-100">
        {props.portal_bool && (
          <Modal
            portal_value={props.portal_value}
            close_portal={props.closePortal}
            dark_mode={props.dark_mode}
          />
        )}
        <div className="row h-100 d-flex justify-content-between">
          <div
            className="col h-100"
            style={
              props.window_width > 1024
                ? { display: "none", paddingTop: "16px" }
                : { display: "block", paddingTop: "16px" }
            }
          >
            <div className="row h-100 d-flex">
              <div
                className={Styles.backBtn}
                style={
                  props.window_width > 767 || props.window_width === 0
                    ? { display: "none" }
                    : { display: "block", marginRight: "5%" }
                }
              >
                <img
                  src={props.dark_mode ? backIconDark : backIcon}
                  alt="icon"
                  className={Styles.backBtn}
                  onClick={() => {
                    props.set_bool_for_content(false);
                  }}
                />
              </div>
              {/* <Tools.ToolsdropContainer
                dark_mode={props.dark_mode}
                checked_arr={props.checked_arr}
                downloadSubmit={props.downloadSubmit}
                svfPath={props.svfPath}
                project_name={props.project_name}
                company_name={props.company_name}
                project_id={props.project_id}
                deleteSubmit={props.deleteSubmit}
                popoverOpen={props.popoverOpen}
                toggle={props.toggle}
                changeYesNoPortal={props.changeYesNoPortal}
                portal_bool_yes_no={props.portal_bool_yes_no}
                portal_value_yes_no={props.portal_value_yes_no}
                function_for_submit={props.function_for_submit}
              /> */}
            </div>
          </div>
          <div
            className="col h-100"
            style={
              props.window_width > 1024
                ? { display: "block", paddingTop: "16px" }
                : { display: "none" }
            }
          >
            <div className="row h-100">
              {/* <Tools.download
                checked_arr={props.checked_arr}
                downloadSubmit={props.downloadSubmit}
                dark_mode={props.dark_mode}
              />

              <Tools.viewer
                checked_arr={props.checked_arr}
                svfPath={props.svfPath}
                company_name={props.company_name}
                project_name={props.project_name}
                dark_mode={props.dark_mode}
                // permission={props.permissions.watch}
              />

              <Tools.compare
                checked_arr={props.checked_arr}
                svfPath={props.svfPath}
                dark_mode={props.dark_mode}
                // permission={props.permissions.compare}
              />

              <Tools.checker
                checked_arr={props.checked_arr}
                company_name={props.company_name}
                project_name={props.project_name}
                project_id={props.project_id}
                dark_mode={props.dark_mode}
                // permission={props.permissions.ils_check_1}
              />
              <Tools.checker_two
                checked_arr={props.checked_arr}
                company_name={props.company_name}
                project_name={props.project_name}
                project_id={props.project_id}
                dark_mode={props.dark_mode}
                // permission={props.permissions.ils_check_2}
              /> */}

              {/* <Tools.drop
                checked_arr={props.checked_arr}
                deleteSubmit={props.deleteSubmit}
                popoverOpen={props.popoverOpen}
                toggle={props.toggle}
                dark_mode={props.dark_mode}
                changeYesNoPortal={props.changeYesNoPortal}
                portal_bool_yes_no={props.portal_bool_yes_no}
                portal_value_yes_no={props.portal_value_yes_no}
                function_for_submit={props.function_for_submit}
              /> */}
            </div>
          </div>
          <div
            className="col d-flex justify-content-end"
            style={{ padding: "0px" }}
          >
            {/* <div
              style={
                props.window_width > 1024
                  ? {
                      width: "18%",
                      maxWidth: "138px",
                    }
                  : props.window_width > 405
                  ? {
                      width: "35px",
                    }
                  : {
                      width: "30px",
                    }
              }
            >
              {!props.permissions.convert_ifc &&
              !props.permissions.convert_ifc ? null : (
                <Tools.format
                  dark_mode={props.dark_mode}
                  user={props.user}
                  project_id={props.project_id}
                />
              )}
            </div> */}
            <div
              className={`${Styles.searcher} ${
                !props.dark_mode ? Styles.searcherLight : ""
              }`}
            >
              <input
                type="text"
                placeholder={props.intl.formatMessage({
                  id: "boilerplate.CompanyPage.searchInput.placeholder",
                })}
                value={props.search_value}
                onChange={props.change_search}
              />
            </div>

            <div style={{ width: props.window_width > 405 ? "35px" : "30px" }}>
              {/* <Tools.mode
                changeViewType={props.changeViewType}
                company_name={props.company_name}
                project_name={props.project_name}
                changePath={props.changePath}
                location={props.location}
                dark_mode={props.dark_mode}
              /> */}
            </div>
            <div
              style={
                props.window_width > 1024
                  ? { width: "110px" }
                  : props.window_width > 405
                  ? { width: "35px" }
                  : { width: "30px" }
              }
            >
              <FormatdropContainer dark_mode={props.dark_mode} />
            </div>
            <div
              style={
                props.window_width > 1024
                  ? {
                      width: "18%",
                      maxWidth: "138px",
                    }
                  : props.window_width > 405
                  ? {
                      width: "35px",
                    }
                  : {
                      width: "30px",
                    }
              }
            >
              {/* <Tools.upload
                onSubmit={props.onSubmit}
                on_change={props.on_change}
                name={props.name}
                point={props.point}
                dark_mode={props.dark_mode}
                // permission={props.permissions.upload}
              /> */}
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="col h-100" style={{ padding: "0px" }}>
        <div className="row h-100 d-flex justify-content-end">
          <div
            className={`col h-100  ${Styles.backBtn}`}
            style={
              props.window_width > 767 || props.window_width === 0
                ? { display: "none" }
                : { display: "block", paddingTop: "16px" }
            }
          >
            <div className={Styles.backBtn}>
              <img
                src={props.dark_mode ? backIconDark : backIcon}
                alt="icon"
                className={Styles.backBtn}
                onClick={() => {
                  props.set_bool_for_content(false);
                }}
              />
            </div>
          </div>
          <div className="col h-100 d-flex justify-content-end">
            {/* <Tools.upload
              onSubmit={props.onSubmit}
              on_change={props.on_change}
              name={props.name}
              point={props.point}
              dark_mode={props.dark_mode}
              // permission={props.permissions.upload}
            /> */}
          </div>
        </div>
      </div>
    );
  }
};

export default Toolbar;
