import React from "react";
import {Field, reduxForm} from "redux-form";

const Form = props => {
  const renderInput = field => (
    <div>
      <input {...field.input} type={field.type} />
      {field.meta.touched && field.meta.error && (
        <span>{field.meta.error}</span>
      )}
    </div>
  );

  return (
    <form onSubmit={props.handleSubmit}>
      <div>
        <Field name="userpic" component={renderInput} type="file" />
      </div>
      <div>
        <button>Send</button>
      </div>
    </form>
  );
};

const FormReduxForm = reduxForm({form: "upload"})(Form);

const UploadForm = props => {
  const onSubmit = formatData => console.log(formatData);

  return (
    <div>
      <FormReduxForm onSubmit={onSubmit} />
    </div>
  );
};

export default UploadForm;
