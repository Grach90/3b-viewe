import * as R from "ramda";

export const files_ids = (set_files_id, checked_arr) =>
  set_files_id(checked_arr ? checked_arr.map(x => x.id) : []);

export const get_svf_path = (set_svfPath, checked_arr) =>
  set_svfPath(
    checked_arr
      .map(x => {
        const tree = x.trees.find(y => y.extension === "svf");
        if (!!tree) {
          return tree.path.split("storage/")[1];
        }
        return null;
      })
      .filter(x => !!x),
  );

export const get_path_for_change = (set_changePath, search, view_type) => {
  const searchPath = search.replace("?", "");
  const types = searchPath !== "" ? searchPath.split("&") : [];
  const getType =
    types.length !== 0 ? types.filter(x => !!x.match(/style/)) : [];

  set_changePath(
    getType.length === 0
      ? types.length === 0
        ? view_type
        : searchPath + "&" + view_type
      : types
          .map(x => {
            if (!!x.match(/style/)) return `${view_type}`;

            return x;
          })
          .join("&"),
  );
};

export const get_project_id = (set_project_id, projects, project_name) =>
  set_project_id(
    R.pipe(R.find(R.propEq("name", project_name)), R.prop("id"))(projects),
  );
