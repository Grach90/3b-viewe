import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

// import {compose} from "ramda";

import Formatdrop from "./index";

// import { changeFormat } from "redux/files/files-actions";

import PropTypes from "prop-types";

const FormatdropContainer = (props) => {
  // const [, company_name, , project_name] = props.match.url.split("/");
  const [bool, setBool] = useState(false);

  // const f_file = props.location.search.replace("?", "");
  // const seachPath = props.location.search.replace("?", "");
  // const types = seachPath !== "" ? seachPath.split("&") : [];

  // const getType =
  //   types.length !== 0 ? types.filter((x) => !!x.match(/type/)) : [];

  // const changePath =
  //   getType.length === 0
  //     ? types.length === 0
  //       ? props.format_file
  //       : seachPath + "&" + props.format_file
  //     : types
  //         .map((x) => {
  //           if (!!x.match(/type/)) {
  //             return `${props.format_file}`;
  //           }

  //           return x;
  //         })
  //         .join("&");

  // const getPath =
  //   props.format_file === "" ? changePath.replace("&", "") : changePath;

  // useEffect(() => () => setBool(false), []);

  // const onChange = (format) => {
  //   setBool(true);
  //   if (format === "ALL") return props.changeFormat("");

  //   const path = `type=${format}`;

  //   return props.changeFormat(path);
  // };
  return (
    <Formatdrop
      format_file={props.format_file}
      // c_name={company_name}
      // p_name={project_name}
      // f_file={f_file}
      bool={bool}
      changeFormat={props.changeFormat}
      // onChange={onChange}
      // getPath={getPath}
      dark_mode={props.dark_mode}
    />
  );
};

// FormatdropContainer.propTypes = {
//   format_file: PropTypes.string,
//   changeFormat: PropTypes.func,
// };

// const mapStateToProps = state => {
//   return {
//     format_file: state.files.format_file,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default compose(
//   connect(mapStateToProps, {changeFormat}),
//   withRouter,
// )(FormatdropContainer);

export default FormatdropContainer;
