import React, { useState, useEffect } from "react";
// import {Redirect} from "react-router-dom";
import { FormattedMessage } from "react-intl";

import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";

import messages from "../../../../messages";

import Styles from "./styles.module.css";
import "./drop.css";

import dropImg from "static/images/dropImg.png";
import dropImgLight from "static/images/dropImg-light.png";
import dropDirection from "static/images/drop-direction.png";
import dropDirectionDark from "static/images/drop-direction-dark.png";
import formatImgLight from "static/images/formatImgLight.png";

const DropFormat = (props) => {
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [dropWindowSize, setDropWindowSize] = useState(true);

  const handleResizeDrop = () => {
    if (window.innerWidth <= 1024) {
      setDropWindowSize(false);
    } else {
      setDropWindowSize(true);
    }
  };

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  // const type =
  //   props.f_file !== ""
  //     ? props.f_file.split("&").filter((x) => !!x.match(/type/))
  //     : [];

  // const file_format = type.length !== 0 ? type[0].split("=")[1] : "";

  useEffect(() => {
    handleResizeDrop();
    window.addEventListener("resize", handleResizeDrop);
    return () => window.removeEventListener("resize", handleResizeDrop);
  }, []);

  return (
    <div className={"h-100 d-flex justify-content-end " + Styles.font}>
      {/* {props.bool && (
        <Redirect
          to={`/${props.c_name}/projects/${props.p_name}?${props.getPath}`}
        />
      )} */}

      <Dropdown
        isOpen={dropdownOpen}
        toggle={toggle}
        className={Styles.dropFormat}
        style={
          window.innerWidth >= 1024
            ? {
                width: "99px",
                height: "35px",
                margin: "16px 8px 0 0",
              }
            : window.innerWidth > 410
            ? {
                width: "35px",
                height: "35px",
                margin: "16px 5px 0 0",
              }
            : {
                width: "30px",
                height: "30px",
                margin: "16px 5px 0 0",
              }
        }
      >
        <DropdownToggle
          caret
          className={Styles.dropToggle}
          style={
            window.innerWidth > 1024
              ? {
                  background: props.dark_mode ? "#111111" : "white",
                  color: props.dark_mode ? "#BFBFBF" : "black",
                  border: "none",
                  width: "99px",
                  height: "35px",
                }
              : window.innerWidth > 410
              ? {
                  backgroundColor: props.dark_mode ? "#111111" : "white",
                  color: props.dark_mode ? "#BFBFBF" : "black",
                  border: "none",
                  width: "35px",
                  height: "35px",
                  fontSize: "12px",
                }
              : {
                  backgroundColor: props.dark_mode ? "#111111" : "white",
                  color: props.dark_mode ? "#BFBFBF" : "black",
                  border: "none",
                  width: "30px",
                  height: "30px",
                  fontSize: "12px",
                }
          }
        >
          {/* {file_format !== "" ? (
            file_format
          ) : dropWindowSize ? ( */}
          <div>
            <FormattedMessage {...messages.formatChangeSelect} />
            <img
              src={props.dark_mode ? dropDirectionDark : dropDirection}
              alt="drop"
              style={{ marginLeft: "5%" }}
            />
          </div>
          {/* ) : (
            <img
              src={props.dark_mode ? dropImg : formatImgLight}
              alt="drop"
              className={Styles.dropImg}
              style={
                window.innerWidth <= 410
                  ? { paddingBottom: "30%" }
                  : { paddingBottom: "0" }
              }
            />
          )} */}
        </DropdownToggle>

        <DropdownMenu
          className={props.dark_mode ? Styles.dropMenu : Styles.dropMenuLight}
          style={
            window.innerWidth <= 600
              ? { left: "-65px", top: "0px", width: "99px" }
              : { top: "35px", width: "99px" }
          }
        >
          <DropdownItem
            className={
              props.dark_mode ? Styles.dropdownItem : Styles.dropdownItemLight
            }
            onClick={() => props.onChange("ALL")}
          >
            <FormattedMessage {...messages.formatChangeAll} />
          </DropdownItem>

          <DropdownItem
            className={
              props.dark_mode ? Styles.dropdownItem : Styles.dropdownItemLight
            }
            onClick={() => props.onChange("RVT")}
          >
            RVT
          </DropdownItem>

          <DropdownItem
            className={
              props.dark_mode ? Styles.dropdownItem : Styles.dropdownItemLight
            }
            onClick={() => props.onChange("IFC")}
          >
            IFC
          </DropdownItem>
        </DropdownMenu>
      </Dropdown>
    </div>
  );
};

export default DropFormat;
