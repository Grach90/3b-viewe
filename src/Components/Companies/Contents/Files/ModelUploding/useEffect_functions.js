import React from "react";
import Styles from "./styles.module.css";

export const get_progress_for_all_files = (
  set_progress_for_all_files,
  in_progress,
) => {
  set_progress_for_all_files(
    in_progress.length !== 0
      ? (in_progress.filter(x => x.status !== "active").length * 100) /
          in_progress.length
      : 0,
  );
};

export const get_count_of_uploading_files = (
  set_count_of_iploading_file,
  in_progress,
) => {
  set_count_of_iploading_file(
    in_progress.length !== 0 ? (
      <span className={Styles.medium}>
        <span className={Styles.bold}>
          {in_progress.filter(x => x.status !== "active").length}
        </span>
        / {in_progress.length}
      </span>
    ) : null,
  );
};

export const get_progress_color = (set_progress_color, dark_mode) => {
  set_progress_color(
    dark_mode
      ? {
          from: "#545454",
          to: "#A5A53E",
        }
      : {
          from: "#B7CAE9",
          to: "#1061EC",
        },
  );
};

export const get_color_for_steps = (set_color_for_steps, status, dark_mode) => {
  if (status === "error" || status === "canceled") {
    set_color_for_steps("#535353");
  } else {
    set_color_for_steps(dark_mode ? "#A5A53E" : "#1061EC");
  }
};

export const get_project_name = (set_project_name, projects, project_id) => {
  set_project_name(projects.find(x => x.id === project_id).name);
};

export const get_short_project_name = (
  set_project_name_short,
  project_name,
) => {
  set_project_name_short(
    project_name.length <= 5
      ? project_name
      : project_name
          .split("")
          .map((x, i) => {
            if (i < 4) {
              return x;
            } else if (i === 5 || i === 6 || i === 7) {
              return ".";
            } else {
              return "";
            }
          })
          .join(""),
  );
};

export const get_file_size = (set_name, file_name) => {
  const name_size = 11;
  set_name(
    file_name.length <= name_size
      ? file_name
      : file_name
          .split("")
          .map((x, i) => {
            if (i < name_size - 3) {
              return x;
            } else if (
              i === name_size ||
              i === name_size - 1 ||
              i === name_size - 2
            ) {
              return ".";
            } else {
              return "";
            }
          })
          .join(""),
  );
};
