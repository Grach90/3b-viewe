import React, { useEffect, useState } from "react";
import { CircularProgressbar } from "react-circular-progressbar";

import Styles from "./styles.module.css";
import "./style-for-progress.css";
import { Spin } from "antd";

import icon_for_cancel from "static/images/Cancel_for_uploading_file.png";
import icon_for_cancel_dark from "static/images/Cancel_for_uploading_file_dark.png";
import done_icon from "static/images/done_uploading_icon.png";
import canceled_upload_icon from "static/images/canceled_upload_icon.png";
import error_upload_icon from "static/images/error_upload_icon.png";
// import {toNumber} from "lodash-es";

import {
  get_color_for_steps,
  get_project_name,
  get_short_project_name,
  get_file_size,
} from "./useEffect_functions";

const UploadingFile = (props) => {
  const [color_for_steps, set_color_for_steps] = useState("");
  const [project_name, set_project_name] = useState("");
  const [project_name_short, set_project_name_short] = useState("");
  const [name, set_name] = useState("");

  useEffect(() => {
    get_color_for_steps(set_color_for_steps, props.status, props.dark_mode);
  }, [props.status, props.dark_mode]);

  useState(() => {
    get_project_name(set_project_name, props.projects, props.project_id);
  }, [props.projects]);

  useEffect(() => {
    get_short_project_name(set_project_name_short, project_name);
  }, [project_name]);

  useEffect(() => {
    get_file_size(set_name, props.file_name);
  }, [props.file_name]);

  const size = (props.size / 1024000).toFixed(2);

  return (
    <div
      className={`d-flex ${props.dark_mode ? Styles.item_dark : Styles.item}`}
    >
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ width: "142px" }}
      >
        <div
          style={{ width: "33px" }}
          className={`${props.dark_mode ? "popover1" : "popover2"}`}
        >
          {first_part(props.status, props.progress, props.dark_mode)}
        </div>
        <div className="d-flex align-items-center">
          <div
            className={Styles.div_for_steps}
            style={{
              marginLeft: "18px",
              background: props.step > 0 ? color_for_steps : "#BFBFBF",
            }}
          ></div>
          <div
            className={Styles.div_for_steps}
            style={{
              background: props.step > 1 ? color_for_steps : "#BFBFBF",
            }}
          ></div>
          <div
            className={Styles.div_for_steps}
            style={{
              background: props.step > 2 ? color_for_steps : "#BFBFBF",
            }}
          ></div>
          <div
            className={Styles.div_for_steps}
            style={{
              background: props.step > 3 ? color_for_steps : "#BFBFBF",
            }}
          ></div>
          <div
            className={`${Styles.div_for_steps} d-flex align-items-center`}
            style={{
              background: props.step > 4 ? color_for_steps : "#BFBFBF",
            }}
          >
            <div></div>
          </div>
        </div>
      </div>
      <div
        className={`d-flex align-items-center justify-content-between ${
          Styles.text
        } ${props.dark_mode ? Styles.text_dark : ""}`}
      >
        <span>
          <span className={Styles.project_name} title={project_name}>
            {project_name_short} /
          </span>
          <span className={Styles.file_name} title={props.file_name}>
            {name}
          </span>
        </span>
        <span
          className={Styles.medium}
          style={{ marginRight: "7px" }}
          title={`${size} MB`}
        >
          {size.length <= 4 ? size : parseInt(size)} MB
        </span>
      </div>
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ width: "45px" }}
      >
        <div
          className="d-flex justify-content-center"
          style={{
            height: "32px",
            borderLeft: "1px solid #EBEBEB",
            width: "100%",
          }}
        >
          <button
            className={Styles.cancel_uploading_file}
            onClick={() =>
              props.click_on_cancel_button(
                props.file_id,
                props.project_id,
                props.step,
                props.status
              )
            }
          >
            {props.bool_for_spin_during_canceling_upload[props.file_id] ? (
              <Spin />
            ) : (
              <img
                src={props.dark_mode ? icon_for_cancel_dark : icon_for_cancel}
                alt="cancel"
              />
            )}
          </button>
        </div>
      </div>
    </div>
  );
};

function first_part(status, progress, dark_mode) {
  switch (status) {
    case "active": {
      return (
        <CircularProgressbar
          striped
          value={progress}
          text={`${progress}%`}
          animated
          now={45}
        />
      );
    }
    case "complated": {
      return (
        <div
          style={{
            background: dark_mode ? "#A5A53E" : "#1061EC",
            width: "31px",
            height: "31px",
            borderRadius: "50%",
          }}
          className="d-flex justify-content-center align-items-center"
        >
          <img src={done_icon} alt="done" />
        </div>
      );
    }
    case "canceled": {
      return (
        <div
          style={{
            background: "#535353",
            width: "31px",
            height: "31px",
            borderRadius: "50%",
          }}
          className="d-flex justify-content-center align-items-center"
          title="You canceled file uploading"
        >
          <img src={canceled_upload_icon} alt="cnceled" />
        </div>
      );
    }
    case "error": {
      return (
        <div
          style={{
            background: "#535353",
            width: "31px",
            height: "31px",
            borderRadius: "50%",
          }}
          className="d-flex justify-content-center align-items-center"
          title="An error occurred while uploading the file"
        >
          <img src={error_upload_icon} alt="error" />
        </div>
      );
    }
    default: {
      return (
        <CircularProgressbar striped value={progress} text={`${progress}%`} />
      );
    }
  }
}

export default UploadingFile;
