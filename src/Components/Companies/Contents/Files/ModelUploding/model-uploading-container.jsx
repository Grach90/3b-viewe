import React, { useEffect, useState } from "react";
import { connect } from "react-redux";

import UploadingFile from "./model-upolading-view";
import cancel_all_uploadig_files from "static/images/Cancel_all_uploadig_files.png";
import cancel_all_uploadig_files_dark from "static/images/cancel_all_uploadig_files_dark.png";
// import {delete_file_from_in_progres} from "redux/files/files-actions";

import { Collapse, Progress } from "antd";
import Styles from "./styles.module.css";
import "antd/dist/antd.css";
import "./style-for-collapse.css";
import "./style-for-progress.css";

// import {
//   get_progress_for_all_files,
//   get_count_of_uploading_files,
//   get_progress_color,
// } from "./useEffect_functions";

import PropTypes from "prop-types";

const { Panel } = Collapse;

const UploadingFileContainer = (props) => {
  const [progress_for_all_files, set_progress_for_all_files] = useState(0);
  const [count_of_iploading_file, set_count_of_iploading_file] = useState(null);
  const [progress_color, set_progress_color] = useState(null);

  // useEffect(() => {
  //   get_progress_for_all_files(set_progress_for_all_files, props.in_progress);
  //   get_count_of_uploading_files(
  //     set_count_of_iploading_file,
  //     props.in_progress
  //   );
  // }, [props.in_progress]);

  // useEffect(() => {
  //   get_progress_color(set_progress_color, props.dark_mode);
  // }, [props.dark_mode]);

  const click_on_cancel_button = (id, project_id, step, status) => {
    if (status === "active") {
      return props.cancel_uploading_file(id, project_id, step);
    }
    return props.delete_file_from_in_progres(id);
  };

  // const models = props.in_progress
  //   .map((file) => (
  //     <UploadingFile
  //       dark_mode={props.dark_mode}
  //       key={file.file_id}
  //       projects={props.projects.filter((x) => !!x.id)}
  //       file_name={file.filename}
  //       file_id={file.file_id}
  //       progress={file.progress}
  //       project_id={file.project_id}
  //       step={file.step}
  //       click_on_cancel_button={click_on_cancel_button}
  //       status={file.status}
  //       size={file.size}
  //       bool_for_spin_during_canceling_upload={
  //         props.bool_for_spin_during_canceling_upload
  //       }
  //     />
  //   ))
  //   .reverse();

  const header = (
    <div className="d-flex justify-content-between align-items-center h-100">
      <span className={Styles.header}>
        Upload files {count_of_iploading_file}
      </span>
      <button
        className={Styles.cancel_all}
        onClick={(event) => {
          event.stopPropagation();
          props.set_open_portal(true);
        }}
      >
        <img
          src={
            props.dark_mode
              ? cancel_all_uploadig_files_dark
              : cancel_all_uploadig_files
          }
          alt="cancel all"
        />
      </button>
    </div>
  );

  return (
    // models.length !== 0 ?
    <div>
      <div
        id={props.dark_mode ? "dark_collapse" : "light_collapse"}
        className={Styles.container}
      >
        <Collapse
          onChange={() => "ddddd"}
          expandIconPosition="left"
          defaultActiveKey={["1"]}
        >
          <Panel header={header} key="1">
            <div
              style={{
                height: "4px",
                background: !props.dark_mode ? "#DBDBDB" : "#3D3D3D",
              }}
            >
              <Progress
                percent={progress_for_all_files}
                status={progress_for_all_files === 100 ? "normal" : "active"}
                showInfo={false}
                strokeColor={progress_color}
                style={{ height: "4px" }}
              />
            </div>
            <div
              className={`${Styles.content_for_uploading_files} ${
                !props.dark_mode ? Styles.dark_content_for_uploading_files : ""
              }`}
            >
              {/* {models} */}
            </div>
          </Panel>
        </Collapse>
      </div>
    </div>
  );
  //  : null;
};

// UploadingFileContainer.propTypes = {
//   projects: PropTypes.array,
//   bool_for_spin_during_canceling_upload: PropTypes.object,
//   in_progress: PropTypes.array,
//   delete_file_from_in_progres: PropTypes.func,
// };
// const mapStateToProps = state => {
//   return {
//     in_progress: state.files.in_progress,
//     projects: state.projects.items,
//     bool_for_spin_during_canceling_upload:
//       state.files.bool_for_spin_during_canceling_upload,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default connect(mapStateToProps, {
//   delete_file_from_in_progres,
// })(UploadingFileContainer);

export default UploadingFileContainer;
