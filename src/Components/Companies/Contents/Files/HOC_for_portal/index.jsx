import React from "react";
import {connect} from "react-redux";
import Yes_no_portal from "../../../../Portal/yes_no_portal";

const Portal = props => {
  return (
    <div>
      <Yes_no_portal
        portal_value={"Are you sure cancel all uploading files"}
        close_portal={() => {
          props.set_open_portal(false);
        }}
        dark_mode={props.dark_mode}
        submit={() => {
          props.cancel_all_uploading_files(props.in_progress);
          props.set_open_portal(false);
        }}
      />
    </div>
  );
};

const MapStateToProps = state => {
  return {
    in_progress: state.files.in_progress,
    dark_mode: state.dark_mode_reducer.mode,
  };
};
export default connect(MapStateToProps, {})(Portal);
