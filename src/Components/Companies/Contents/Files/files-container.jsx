import { useTypedSelector } from "hooks/useTypedSelector";
import React, { useEffect, useState } from "react";
// import { compose } from "redux";
// import { connect } from "react-redux";
import { useParams } from "react-router-dom";
// import {useDropzone} from "react-dropzone";

// import PropTypes from "prop-types";

import FilesView from "./files-view";

// import {
//   changeViewType,
//   emptyCheckArr,
//   fetchFiles,
//   get_progress,
//   nullify_error_during_uploading,
//   cancele_model_creating,
//   cancel_uploading,
//   set_value_of_spin_during_canceling_upload,
//   delete_all_from_in_progress,
// } from "redux/files/files-actions";

import {
  sorted_file_lis,
  // get_id,
  // get_project_id,
  // get_searched_files,
  // get_format_of_file,
  // get_style_format,
  // get_error_during_uploading,
  // get_socket_progress,
  // get_files,
} from "./useEffect_functions";

// import { update_members_rights } from "../../../../redux/admin/admin-actions";

const FilesContainer = (props) => {
  const { list_files } = useTypedSelector((state) => state.filesState);

  const { items } = useTypedSelector((state) => state.projectState);
  // const { project_name } = useParams();
  // console.log(useParams());

  const [window_width, setWindowWidth] = useState(0);
  // const { acceptedFiles, getRootProps } = useDropzone();
  const [modal, setModal] = useState(false);
  const [error_messages, set_error_messages] = useState([]);
  const [open_portal, set_open_portal] = useState(false);
  // const [project_name, set_project_name] = useState("");
  const [id, set_id] = useState(undefined);
  const [project_id, set_project_id] = useState("");
  const [search_files, set_search_files] = useState([]);
  const [list_files_sorting, set_list_files_sorting] = useState([]);
  const [file_format, set_file_format] = useState("");
  const [style_of_files, set_style_of_files] = useState("");

  useEffect(
    () => sorted_file_lis(list_files, set_list_files_sorting),
    [props.list_files]
  );

  // useEffect(() => {
  //   set_project_name(props.match.url.split("/").pop());
  // }, [props.match.url]);

  // useEffect(() => {
  //   get_id(set_id, props.list_projects, project_name);
  //   get_project_id(set_project_id, props.list_projects, project_name);
  // }, [props.list_projects, project_name]);

  // useEffect(() => {
  //   get_searched_files(
  //     set_search_files,
  //     props.search_value,
  //     list_files_sorting
  //   );
  // }, [props.search_value, list_files_sorting]);

  // useEffect(() => {
  //   get_format_of_file(set_file_format, props.location.search);
  //   get_style_format(set_style_of_files, props.location.search);
  // }, [props.location.search]);

  // useEffect(() => {
  //   get_error_during_uploading(
  //     props.error_during_uploading,
  //     setModal,
  //     set_error_messages
  //   );
  // }, [props.error_during_uploading]);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
  }, [window.innerWidth]);

  // useEffect(() => {
  //   get_files(props.emptyCheckArr, id, props.fetchFiles);
  // }, [id, props.change_value]);

  // useEffect(() => {
  //   get_socket_progress(props.socket, props.get_progress);
  // }, []);

  const toggle = () => {
    props.nullify_error_during_uploading();
    setModal(!modal);
  };

  const cancel_uploading_file = (id, project_id, step) => {
    props.set_value_of_spin_during_canceling_upload(id, true);
    if (step > 1) return props.cancele_model_creating(project_id, id);
    props.list_upload_canceling_files[id].cancel("Cancel uploading file");
    return props.cancel_uploading(id);
  };

  // useEffect(() => {
  //   if (project_name) {
  //     if (
  //       props.user.role === "3B Manager" ||
  //       props.user.role === "Project Manager"
  //     ) {
  //       props.update_members_rights(project_name, props.user.id, {
  //         upload: true,
  //         convert_ifc: true,
  //         convert_obj: true,
  //         create_issues: true,
  //         create_markup: true,
  //         compare: true,
  //         watch: true,
  //         ils_check_1: true,
  //         ils_check_2: true,
  //       });
  //     }
  //   }
  // }, [project_name]);

  const cancel_all_uploading_files = (files) => {
    if (files.find((x) => x.status === "active")) {
      files.forEach((x) => {
        if (x.status === "active") {
          cancel_uploading_file(x.file_id, x.project_id, x.step);
        }
      });
    } else {
      props.delete_all_from_in_progress();
    }
  };
  return (
    <FilesView
      list_files={list_files_sorting}
      changeViewType={props.changeViewType}
      emptyCheckArr={props.emptyCheckArr}
      is_fetching={props.is_fetching}
      file_format={file_format}
      project_id={project_id}
      style_of_files={style_of_files}
      dark_mode={props.dark_mode}
      search_value={props.search_value}
      checked_arr={props.checked_arr}
      set_bool_for_content={props.set_bool_for_content}
      error_during_uploading={props.error_during_uploading}
      nullify_error_during_uploading={props.nullify_error_during_uploading}
      cancel_uploading_file={cancel_uploading_file}
      cancel_all_uploading_files={cancel_all_uploading_files}
      window_width={window_width}
      // acceptedFiles={acceptedFiles}
      // getRootProps={getRootProps}
      modal={modal}
      error_messages={error_messages}
      open_portal={open_portal}
      set_open_portal={set_open_portal}
      toggle={toggle}
      search_files={search_files}
      permissions={props.user_permissions}
    />
  );
};

// FilesContainer.propTypes = {
//   match: PropTypes.object,
//   location: PropTypes.object,
//   list_projects: PropTypes.array,
//   in_progress: PropTypes.array,
//   list_files: PropTypes.array,
//   change_value: PropTypes.number,
//   checked_arr: PropTypes.array,
//   is_fetching: PropTypes.bool,
//   search_value: PropTypes.string,
//   search_value: PropTypes.string,
//   error_during_uploading: PropTypes.array,
//   list_upload_canceling_files: PropTypes.object,
//   socket: PropTypes.object,
//   room: PropTypes.string,
//   fetchFiles: PropTypes.func,
//   emptyCheckArr: PropTypes.func,
//   changeViewType: PropTypes.func,
//   changeViewType: PropTypes.func,
//   get_progress: PropTypes.func,
//   nullify_error_during_uploading: PropTypes.func,
//   cancele_model_creating: PropTypes.func,
//   cancel_uploading: PropTypes.func,
//   set_value_of_spin_during_canceling_upload: PropTypes.func,
//   delete_all_from_in_progress: PropTypes.func,
// };

// const mapStateToProps = (state) => {
//   return {
//     user: state.session.user,
//     list_projects: state.projects.items,
//     list_files: state.files.list_files,
//     change_value: state.files.changeValue,
//     is_fetching: state.files.isFetching,
//     search_value: state.files.search,
//     checked_arr: state.files.checkedArr,
//     error_during_uploading: state.files.error_during_uploading,
//     list_upload_canceling_files: state.files.list_upload_canceling_files,
//     user_permissions: state.projects.user_permissions,
//     // Socket
//     socket: state.io.socket,
//     room: state.io.room,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default compose(
//   connect(mapStateToProps, {
//     changeViewType,
//     emptyCheckArr,
//     fetchFiles,
//     get_progress,
//     nullify_error_during_uploading,
//     cancele_model_creating,
//     cancel_uploading,
//     set_value_of_spin_during_canceling_upload,
//     delete_all_from_in_progress,
//     update_members_rights,
//   }),
//   withRouter
// )(FilesContainer);

export default FilesContainer;
