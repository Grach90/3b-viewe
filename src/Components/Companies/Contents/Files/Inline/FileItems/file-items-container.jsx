import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import React, { useEffect, useState } from "react";

import FileItemsView from "./file-items-view";

import { get_image } from "./useEffect_functons";

const FileItemsContainer = (props) => {
  const [window_width, setWindowWidth] = useState(0);
  // const [file_size, set_file_size] = useState(0);
  const [image, set_image] = useState("");
  const { dark_mode } = useTypedSelector((state) => state.globalState);
  const { CheckedFiles } = useActions();

  useEffect(() => {
    setWindowWidth(window.innerWidth);
  }, [window.innerWidth]);

  // useEffect(() => get_size_of_files(props.file, set_file_size), []);

  useEffect(
    () => get_image(props.file, dark_mode, set_image),
    [props.file, dark_mode]
  );

  const addCheckedFile = (id) => CheckedFiles(id);

  return (
    <div>
      <FileItemsView
        addCheckedFile={addCheckedFile}
        name={props.file.file_name}
        checked_arr={props.checked_arr}
        file={props.file}
        download_file={props.download_file}
        dark_mode={dark_mode}
        chekedProperty={props.chekedProperty}
        // chekedProperty={props.chekedProperty}
        window_width={window_width}
        image={image}
        // file_size={file_size}
        name_width={props.name_width}
        created_width={props.created_width}
        size_width={props.size_width}
        type_width={props.type_width}
        version_width={props.version_width}
        issue_width={props.issue_width}
      />
    </div>
  );
};

export default FileItemsContainer;
