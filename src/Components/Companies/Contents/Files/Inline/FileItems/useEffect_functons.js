import fileIcon from "static/images/fileIcon.png";
import fileIconLight from "static/images/RvtLogoLight.png";
import ifcIcon from "static/images/ifcInline.png";
import ifcIconLight from "static/images/IfcLOgoLight.png";

// export const get_size_of_files = (file, set_file_size) => {
//   const allowed_extensions = ["rvt", "ifc", "nwc", "iam", "ipt"];
//   const size =
//     file.trees.find((x) => allowed_extensions.some((y) => y === x.extension))
//       .size / 1024000;

//   return set_file_size(size.toFixed(2));
// };

export const get_image = (file, dark_mode, set_image) => {
  const file_category = !!file.trees
    ? file.trees.filter((x) => x.extension !== "svf")[0]
    : "";

  set_image(
    file_category.extension === "rvt"
      ? dark_mode
        ? fileIcon
        : fileIconLight
      : dark_mode
      ? ifcIcon
      : ifcIconLight
  );
};

export const get_file_name = (setFilename, file) => {
  const parent_size = document.getElementById("fileName")
    ? document.getElementById("fileName").offsetWidth
    : window.innerWidth / 10;
  const size = parseInt((parent_size * 12) / 100);

  const arr_name_without_extension = file.filename.split(".");
  arr_name_without_extension.pop();
  const name_without_extension = arr_name_without_extension.join(".");

  const name =
    name_without_extension.length <= size
      ? name_without_extension
      : name_without_extension
          .split("")
          .map((x, i) => {
            if (i < size - 3) {
              return x;
            } else if (i === size || i === size - 1 || i === size - 2) {
              return ".";
            } else {
              return "";
            }
          })
          .join("");
  setFilename(name);
};
