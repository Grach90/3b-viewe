import React, { useRef } from "react";

import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import Styles from "./styles.module.css";

import { ReflexContainer, ReflexSplitter, ReflexElement } from "react-reflex";

import "react-reflex/styles.css";
import "../reflex_styles.css";

import issueIcon from "static/images/issueIcon.png";
import issueIconLight from "static/images/issueIconLight.png";

const FileItemsView = (props) => {
  const isClick = useRef(true);
  return (
    <div
      className={"row " + Styles.file}
      style={{
        height: "68px",
        // background: props.checked_arr.includes(props.file)
        //   ? props.dark_mode
        //     ? "#232323"
        //     : "#D9D9D9"
        //   : "",
      }}
      onClick={() => {
        if (!isClick.current) {
          isClick.current = true;
          return;
        }
        return props.addCheckedFile(props.file.id);
      }}
    >
      <div className="col h-100">
        <div className={"row " + Styles.tableHead}>
          <ReflexContainer
            orientation="vertical"
            style={{
              height: "100%",
              color: "black",
              borderBottom: !props.dark_mode
                ? "1px solid #C8C8C8"
                : "1px solid #484848",
            }}
            className="d-flex align-items-center"
          >
            <ReflexElement
              className="left-pane d-flex"
              // size={props.name_width}
              // minSize="300"
            >
              <div className="pane-content">
                <div className="d-flex align-items-center h-100">
                  <label className={Styles.container}>
                    <input
                      type="checkbox"
                      checked={props.checked_arr.includes(props.file.id)}
                      onChange={() => props.addCheckedFile(props.file.id)}
                    />

                    <span
                      className={
                        props.dark_mode
                          ? Styles.checkmark
                          : Styles.checkmarkLight
                      }
                    />
                  </label>

                  <h6
                    className={Styles.logo}
                    id="fileName"
                    // title={props.file.filename}
                    style={{
                      color: props.dark_mode ? "#7D7D7D" : "black",
                      overflow: "hidden",
                    }}
                  >
                    {/* {props.download_file[props.file.id] ? (
                      props.download_file[props.file.id].bool ? (
                        <div className="d-flex justify-content-center align-items-center">
                          <div
                            className={` ${
                              props.dark_mode ? "popover1" : "popover2"
                            }`}
                            style={{
                              width: "46px",
                              color: "#858585",
                              marginTop: "-3px",
                            }}
                          >
                            <CircularProgressbar
                              striped
                              value={
                                props.download_file[props.file.id]
                                  .progressForDownload
                              }
                              text={`${
                                props.download_file[props.file.id]
                                  .progressForDownload
                              }%`}
                            />
                          </div>

                          <h6
                            className={"w-100 " + Styles.logo1}
                            style={{
                              fontWeight: "bold",
                              color: props.dark_mode ? "white" : "black",
                            }}
                          >
                            <span
                              className={"pl-2 " + Styles.bold}
                              style={{
                                height: "14px",
                                overflow: "hidden",
                              }}
                            >
                              {props.name.slice(0, -4)}
                            </span>
                          </h6>
                        </div>
                      ) : (
                        <span style={{ display: "inline-flex" }}>
                          <img
                            src={props.image}
                            className={Styles.icon}
                            alt=""
                          />

                          <span
                            className={"pl-2 " + Styles.bold}
                            style={{
                              height: "14px",
                              overflow: "hidden",
                            }}
                          >
                            {props.name.slice(0, -4)}
                          </span>
                        </span>
                      )*/}
                    {/* ) : (  */}
                    <span className="d-flex align-items-center">
                      <img src={props.image} className={Styles.icon} alt="" />

                      <span
                        className={"pl-2 " + Styles.bold}
                        style={{
                          height: "14px",
                          overflow: "hidden",
                        }}
                      >
                        {props.file.file_name}
                      </span>
                    </span>
                  </h6>
                </div>
              </div>
            </ReflexElement>

            <ReflexSplitter
              propagate={true}
              // onStartResize={(e) => {
              //   isClick.current = false;
              // }}
            />

            {props.window_width >= 850 && (
              <ReflexElement
                className="middle-pane d-flex align-items-center"
                // minSize="60"
                // size={props.size_width}
              >
                <div className="pane-content">
                  <h6
                    className={Styles.headers}
                    style={{
                      color: props.dark_mode ? "#7D7D7D" : "black",
                    }}
                  >
                    {props.file.file_size} MB
                  </h6>
                </div>
              </ReflexElement>
            )}
            {props.window_width >= 850 && (
              <ReflexSplitter
                propagate={true}
                // onStartResize={(e) => {
                //   isClick.current = false;
                // }}
              />
            )}
            {props.window_width >= 1000 && (
              <ReflexElement
                className="middle-pane d-flex align-items-center"
                // minSize="80"
                // size={props.created_width}
              >
                <div className="pane-content">
                  <h6
                    className={Styles.headers}
                    style={{
                      width: props.window_width <= 1120 ? "19%" : "13%",
                      // minWidth: "80px",
                      color: props.dark_mode ? "#7D7D7D" : "black",
                    }}
                  >
                    {props.file.created_at.split("T")[0]}
                  </h6>
                </div>
              </ReflexElement>
            )}
            {props.window_width >= 1000 && (
              <ReflexSplitter
                propagate={true}
                onStartResize={(e) => {
                  isClick.current = false;
                }}
              />
            )}
            <ReflexElement
              className="middle-pane d-flex align-items-center"
              // minSize="50"
              // size={props.type_width}
              //maxSize="200"
            >
              <div className="pane-content">
                <h6
                  className={Styles.headers}
                  style={{
                    width: props.window_width <= 1120 ? "15%" : "13%",
                    minWidth: "80px",
                    color: props.dark_mode ? "#7D7D7D" : "black",
                  }}
                >
                  {props.file.extension}
                </h6>
              </div>
            </ReflexElement>

            <ReflexSplitter
              propagate={true}
              onStartResize={(e) => {
                isClick.current = false;
              }}
            />
            <ReflexElement
              className="middle-pane d-flex align-items-center"
              // minSize="70"
              // size={props.version_width}
              //maxSize="200"
            >
              <div className="pane-content">
                <h6
                  className={Styles.headers}
                  style={{
                    width: props.window_width <= 1120 ? "18%" : "10%",
                    minWidth: "80px",
                    color: props.dark_mode ? "#7D7D7D" : "black",
                  }}
                >
                  {props.file.version}
                </h6>
              </div>
            </ReflexElement>

            {/* {props.window_width >= 1300 && (
              <ReflexSplitter
                propagate={true}
                onStartResize={(e) => {
                  isClick.current = false;
                }}
              />
            )} */}
            {/* {props.window_width >= 1300 && (
              <ReflexElement
                className="middle-pane d-flex align-items-center"
                minSize="60"
                size={props.issue_width}
              >
                <div className="pane-content">
                  <h6
                    className={Styles.headers}
                    style={{
                      color: props.dark_mode ? "#7D7D7D" : "black",
                      paddingTop: "13px",
                    }}
                  >
                    <img
                      src={props.dark_mode ? issueIcon : issueIconLight}
                      style={{ marginRight: "6px" }}
                      // style={{float: "left"}}
                      z
                      alt=""
                    />
                    <div style={{ margin: "2px 0px 0px 25px" }}>
                      {props.file.pushpins_count}
                    </div>
                  </h6>
                </div>
              </ReflexElement>
            )} */}
            {/* 
            {props.window_width >= 1300 && (
              <ReflexSplitter
                propagate={true}
                onStartResize={(e) => {
                  isClick.current = false;
                }}
              />
            )}
            {props.window_width >= 1300 && (
              <ReflexElement
                className="right-pane d-flex align-items-center"
                minSize="80"
              >
                <div className="pane-content">
                  <h6
                    className={Styles.headers}
                    style={{
                      color: props.dark_mode ? "#7D7D7D" : "black",
                    }}
                  >
                    {props.file.markups_count}
                  </h6>
                </div>
              </ReflexElement>
            )} */}
          </ReflexContainer>
        </div>
      </div>
    </div>
  );
};

export default FileItemsView;
