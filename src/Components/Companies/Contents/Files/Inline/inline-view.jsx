import React, { useEffect, useState, useRef } from "react";
import { FormattedMessage } from "react-intl";

import FileItems from "./FileItems";
import messages from "../../../messages";
import Styles from "./styles.module.css";

import preloader from "static/preloader/Gear-0.2s-200pxfile.svg";

import { ReflexContainer, ReflexSplitter, ReflexElement } from "react-reflex";

import "react-reflex/styles.css";
import "./reflex_styles.css";
import { useTypedSelector } from "hooks/useTypedSelector";

const Name = (props) => {
  useEffect(() => {
    if (typeof props.dimensions.width === "number")
      props.set_name_width(props.dimensions.width);
  }, [props.dimensions.width]);

  return (
    <div className="pane-content h-100">
      <div className="d-flex align-items-center h-100">
        <label className={`${Styles.container} `}>
          <input
            type="checkbox"
            checked={props.all_checked_bool}
            onChange={() => props.AllCheckedFiles()}
          />

          <span
            className={
              props.dark_mode ? Styles.checkmark : Styles.checkmarkLight
            }
          />
        </label>
        <h4
          className={`${Styles.headers} d-flex align-items-center`}
          style={{
            color: props.dark_mode ? "white" : "black",
            borderLeft: "2px solid #C8C8C8",
          }}
        >
          <FormattedMessage {...messages.fileName} />
        </h4>
      </div>
    </div>
  );
};
const Size = (props) => {
  useEffect(() => {
    if (typeof props.dimensions.width === "number")
      props.set_size_width(props.dimensions.width);
  }, [props.dimensions.width]);

  return (
    <div className="pane-content h-100">
      <h4
        className={`${Styles.headers} d-flex align-items-center Size  h-100`}
        style={{
          color: props.dark_mode ? "white" : "black",
        }}
      >
        <FormattedMessage {...messages.fileSize} />
      </h4>
    </div>
  );
};
const Created = (props) => {
  useEffect(() => {
    if (typeof props.dimensions.width === "number")
      props.set_created_width(props.dimensions.width);
  }, [props.dimensions.width]);

  return (
    <div className="pane-content h-100">
      <h4
        className={`${Styles.headers} Created  d-flex align-items-center h-100`}
        style={{
          color: props.dark_mode ? "white" : "black",
        }}
      >
        <FormattedMessage {...messages.fileCreated} />
      </h4>
    </div>
  );
};
const Type = (props) => {
  useEffect(() => {
    if (typeof props.dimensions.width === "number")
      props.set_type_width(props.dimensions.width);
  }, [props.dimensions.width]);

  return (
    <div className="pane-content  h-100">
      <h4
        className={`${Styles.headers} d-flex align-items-center Type  h-100`}
        style={{
          color: props.dark_mode ? "white" : "black",
        }}
      >
        <FormattedMessage {...messages.fileType} />
      </h4>
    </div>
  );
};
const Version = (props) => {
  useEffect(() => {
    if (typeof props.dimensions.width === "number")
      props.set_version_width(props.dimensions.width);
  }, [props.dimensions.width]);

  return (
    <div className="pane-content h-100">
      <h4
        className={`${Styles.headers} d-flex align-items-center Version  h-100`}
        style={{
          color: props.dark_mode ? "white" : "black",
        }}
      >
        <FormattedMessage {...messages.fileVersion} />
      </h4>
    </div>
  );
};
const Issue = (props) => {
  useEffect(() => {
    if (typeof props.dimensions.width === "number")
      props.set_issue_width(props.dimensions.width);
  }, [props.dimensions.width]);

  return (
    <div className="pane-content h-100">
      <h4
        className={`${Styles.headers} d-flex align-items-center Issues  h-100`}
        style={{
          color: props.dark_mode ? "white" : "black",
        }}
      >
        <FormattedMessage {...messages.issueCount} />
      </h4>
    </div>
  );
};
const MarkupContainer = (props) => {
  return (
    <div className="pane-content h-100">
      <h4
        className={`${Styles.headers} d-flex align-items-center Markups h-100`}
        style={{
          color: props.dark_mode ? "white" : "black",
        }}
      >
        <FormattedMessage {...messages.markupCount} />
      </h4>
    </div>
  );
};

const InlineView = (props) => {
  const [name_width, set_name_width] = useState(300);
  const [created_width, set_created_width] = useState(300);
  const [size_width, set_size_width] = useState(300);
  const [type_width, set_type_width] = useState(300);
  const [version_width, set_version_width] = useState(300);
  const [issue_width, set_issue_width] = useState(300);
  const { list_files, checkedArr } = useTypedSelector(
    (state) => state.filesState
  );
  const files_for_inline_style = list_files.map((file, i) => {
    return (
      <FileItems
        file={file}
        key={file.id}
        checkedArrFun={props.checkedArrFun}
        checked_arr={checkedArr}
        receiveFiles={props.receiveFiles}
        download_file={props.download_file}
        dark_mode={props.dark_mode}
        chekedProperty={props.chekedProperty}
        name_width={name_width}
        created_width={created_width}
        size_width={size_width}
        type_width={type_width}
        version_width={version_width}
        issue_width={issue_width}
      />
    );
  });
  return (
    <div className="col h-100">
      <div
        className={"row " + Styles.tableHead}
        style={{ background: props.dark_mode ? "black" : "white" }}
      >
        <div className={"col"}>
          <div className="row h-100">
            <ReflexContainer
              orientation="vertical"
              style={{
                height: "100%",
                color: "black",
                borderBottom: !props.dark_mode ? "1px solid #C8C8C8" : "none",
              }}
              className="d-flex align-items-center"
            >
              <ReflexElement
                className="left-pane d-flex"
                minSize="300"
                propagateDimensionsRate={500}
                propagateDimensions={true}
              >
                <Name {...props} set_name_width={set_name_width} />
              </ReflexElement>
              <ReflexSplitter propagate={false} />
              {props.window_width >= 850 && (
                <ReflexElement
                  className="middle-pane d-flex align-items-center"
                  minSize="60"
                  propagateDimensionsRate={500}
                  propagateDimensions={true}
                >
                  <Size
                    dark_mode={props.dark_mode}
                    set_size_width={set_size_width}
                  />
                </ReflexElement>
              )}
              {props.window_width >= 850 && (
                <ReflexSplitter propagate={false} />
              )}
              {props.window_width >= 1000 && (
                <ReflexElement
                  className="middle-pane d-flex align-items-center"
                  minSize="80"
                  propagateDimensionsRate={500}
                  propagateDimensions={true}
                >
                  <Created
                    set_created_width={set_created_width}
                    dark_mode={props.dark_mode}
                  />
                </ReflexElement>
              )}
              {props.window_width >= 1000 && (
                <ReflexSplitter propagate={false} />
              )}
              <ReflexElement
                className="middle-pane d-flex align-items-center"
                minSize="50"
                propagateDimensionsRate={500}
                propagateDimensions={true}
              >
                <Type
                  dark_mode={props.dark_mode}
                  set_type_width={set_type_width}
                />
              </ReflexElement>
              <ReflexSplitter propagate={false} />
              <ReflexElement
                className="middle-pane d-flex align-items-center"
                minSize="70"
                propagateDimensionsRate={500}
                propagateDimensions={true}
              >
                <Version
                  dark_mode={props.dark_mode}
                  set_version_width={set_version_width}
                />
              </ReflexElement>
              {/* {props.window_width >= 1300 && (
                <ReflexSplitter propagate={false} />
              )}
              {props.window_width >= 1300 && (
                <ReflexElement
                  className="middle-pane d-flex align-items-center"
                  minSize="60"
                  propagateDimensionsRate={500}
                  propagateDimensions={true}
                >
                  <Issue
                    dark_mode={props.dark_mode}
                    set_issue_width={set_issue_width}
                  />
                </ReflexElement>
              )}
              {props.window_width >= 1300 && (
                <ReflexSplitter propagate={false} />
              )}
              {props.window_width >= 1300 && (
                <ReflexElement
                  className="right-pane d-flex align-items-center"
                  minSize="80"
                  propagateDimensionsRate={500}
                  propagateDimensions={true}
                >
                  <MarkupContainer dark_mode={props.dark_mode} />
                </ReflexElement>
              )} */}
            </ReflexContainer>
          </div>
        </div>
      </div>

      <div className={"row " + Styles.items}>
        <div
          className={`col h-100 ${Styles.scroll} ${
            !props.dark_mode ? Styles.scroll_dark : ""
          }`}
        >
          {props.is_fetching && <img src={preloader} alt="" />}

          {files_for_inline_style}
        </div>
      </div>
    </div>
  );
};

export default InlineView;
