export const get_size_of_window = (setWindowWidth, setDropdownOpen, window) => {
  if (window.innerWidth <= 375 || window.innerWidth > 1024) {
    setDropdownOpen(false);
  }
  setWindowWidth(window.innerWidth);
};

export const files_without_uploading_files = (
  set_without_uploading_file,
  list_files,
  in_progress,
) => {
  set_without_uploading_file(
    list_files.filter(
      x =>
        !in_progress.find(y => y.file_id === x.id && y.status !== "complated"),
    ),
  );
};
