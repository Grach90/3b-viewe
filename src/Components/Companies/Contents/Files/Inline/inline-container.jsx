import { useActions } from "hooks/useActions";
import React, { useEffect, useState } from "react";
// import { connect } from "react-redux";
// import PropTypes from "prop-types";

import InlineView from "./inline-view";

// import {
//   checkedArrFun,
//   receiveFiles,
//   allChecked,
//   cancele_model_creating,
// } from "redux/files/files-actions";

import {
  get_size_of_window,
  files_without_uploading_files,
} from "./useEffect_functions";

const InlineContainer = (props) => {
  const [window_width, setWindowWidth] = useState(0);
  const [chekedProperty, setProperty] = useState();
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [without_uploading_file, set_without_uploading_file] = useState([]);
  const { AllCheckedFiles } = useActions();

  useEffect(() => {
    get_size_of_window(setWindowWidth, setDropdownOpen, window);
    setProperty((prev) => (prev ? prev : "Size"));
  }, [window.innerWidth]);

  useEffect(() => {
    files_without_uploading_files(
      set_without_uploading_file,
      props.list_files,
      props.in_progress
    );
  }, [props.list_files, props.in_progress]);

  const toggle = () => setDropdownOpen((prevState) => !prevState);
  return (
    <InlineView
      // For files
      checkedArrFun={props.checkedArrFun}
      receiveFiles={props.receiveFiles}
      download_file={props.download_file}
      all_checked_bool={props.all_checked_bool}
      AllCheckedFiles={AllCheckedFiles}
      // From "../files-view"
      list_files={props.list_files}
      checked_arr={props.checked_arr}
      // in_progress={props.in_progress}
      project_id={props.project_id}
      dark_mode={props.dark_mode}
      cancele_model_creating={props.cancele_model_creating}
      window_width={props.window_width}
      //
      without_uploading_file={without_uploading_file}
      // window_width={window_width}
      chekedProperty={chekedProperty}
      dropdownOpen={dropdownOpen}
      setProperty={setProperty}
      toggle={toggle}
    />
  );
};

// InlineContainer.propTypes = {
//   download_file: PropTypes.object,
//   checked_arr: PropTypes.array,
//   all_checked_bool: PropTypes.bool,
//   in_progress: PropTypes.array,
//   allChecked: PropTypes.func,
//   checkedArrFun: PropTypes.func,
//   receiveFiles: PropTypes.func,
//   cancele_model_creating: PropTypes.func,
// };

// const mapStateToProps = (state) => {
//   return {
//     download_file: state.files.downloadFile,
//     all_checked_bool: state.files.allchecked,
//     in_progress: state.files.in_progress,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default connect(mapStateToProps, {
//   // For files
//   checkedArrFun,
//   receiveFiles,
//   allChecked,
//   cancele_model_creating,
// })(InlineContainer);

export default InlineContainer;
