import React from "react";
import { FormattedMessage } from "react-intl";

import ToolBar from "./Toolbar";
import Inline from "./Inline";
import Cup from "./Cup";
import messages from "../../messages";

import Styles from "./styles.module.css";
import Portal from "./HOC_for_portal";

import preloader from "static/preloader/Gear-0.2s-200pxfile.svg";
import preloaderLight from "static/preloader/preloaderLight.svg";
import foldIcon from "static/images/icons8-folder-128 (3).png";
import foldIconLight from "static/images/lightEmptyProject.png";
import UploadingFileContainer from "./ModelUploding";

import { Modal, ModalBody } from "reactstrap";

const FilesView = (props) => {
  return (
    <div className={Styles.container}>
      {!props.list_files || props.list_files.length === 0 ? (
        <div className="w-100 h-100">
          <div
            className={`w-100 ${
              props.dark_mode ? Styles.content : Styles.contentLight
            }`}
          >
            <ToolBar
              dark_mode={props.dark_mode}
              set_bool_for_content={props.set_bool_for_content}
              acceptedFiles={props.acceptedFiles}
              permissions={props.permissions}
            />
          </div>

          <div
            className={"row w-100 mt-3 mx-auto " + Styles.body}
            style={{
              backgroundColor: props.dark_mode ? "#141414" : "white",
              boxShadow: props.dark_mode ? "none" : "0px 0px 7px -2px",
            }}
            // {...props.getRootProps()}
          >
            <div className="col-4  m-auto">
              <div className="row d-flex justify-content-center">
                {props.is_fetching ? (
                  <img
                    src={props.dark_mode ? preloader : preloaderLight}
                    style={{ width: "100px" }}
                    alt=""
                  />
                ) : (
                  <img
                    src={props.dark_mode ? foldIcon : foldIconLight}
                    alt="folder"
                    className="mx-auto"
                    style={{ width: "86px" }}
                  />
                )}
              </div>

              <div className="row">
                {!props.is_fetching && (
                  <h3
                    className={"mx-auto " + Styles.font}
                    style={{
                      fontSize: "20px",
                      color: props.dark_mode ? "#808080" : "black",
                      textAlign: "center",
                    }}
                  >
                    <FormattedMessage {...messages.emptyProject} />
                  </h3>
                )}
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="w-100 h-100">
          <div
            className={"w-100 " + Styles.content}
            style={{
              borderBottom: props.dark_mode
                ? "2px solid black"
                : "2px solid #B9B9B9",
            }}
          >
            <ToolBar
              list_files={props.list_files}
              changeViewType={props.changeViewType}
              project_id={props.project_id}
              dark_mode={props.dark_mode}
              set_bool_for_content={props.set_bool_for_content}
              acceptedFiles={props.acceptedFiles}
              permissions={props.permissions}
            />
          </div>

          <div
            className={`w-100  ${
              !props.dark_mode &&
              (props.style_of_files === "inline" || !props.style_of_files)
                ? Styles.inlineDivLight
                : Styles.inlineDiv
            }`}
            // {...props.getRootProps()}
          >
            <div
              className={`w-100  ${Styles.inlineDivChild}`}
              style={{
                display:
                  props.style_of_files === "inline" || !props.style_of_files
                    ? "block"
                    : "none",
                background: props.dark_mode ? "#141414" : "white",
              }}
            >
              <Inline
                checked_arr={props.checked_arr}
                is_fetching={props.is_fetching}
                befor_uploading={props.befor_uploading}
                dark_mode={props.dark_mode}
                window_width={props.window_width}
                project_id={props.project_id}
                list_files={
                  props.file_format === ""
                    ? props.search_files
                    : props.search_files.filter(
                        (x) =>
                          x.trees.filter((y) => y.extension !== "svf")[0]
                            .extension === props.file_format.toLowerCase()
                      )
                }
              />
            </div>

            <div
              className={Styles.cup}
              style={{
                display: props.style_of_files === "grid" ? "block" : "none",
              }}
            >
              <Cup
                checked_arr={props.checked_arr}
                dark_mode={props.dark_mode}
                window_width={props.window_width}
                project_id={props.project_id}
                list_files={
                  props.file_format === ""
                    ? props.search_files
                    : props.search_files.filter(
                        (x) =>
                          x.trees.filter((y) => y.extension !== "svf")[0]
                            .extension == props.file_format.toLowerCase()
                      )
                }
              />
            </div>
          </div>
        </div>
      )}

      <UploadingFileContainer
        dark_mode={props.dark_mode}
        cancel_uploading_file={props.cancel_uploading_file}
        set_open_portal={props.set_open_portal}
      />
      <Modal
        isOpen={props.modal}
        toggle={props.toggle}
        style={{ borderRadius: "5px" }}
      >
        <ModalBody className="d-flex align-items-center">
          <div>{props.error_messages}</div>
        </ModalBody>
      </Modal>
      {props.open_portal && (
        <Portal
          cancel_all_uploading_files={props.cancel_all_uploading_files}
          set_open_portal={props.set_open_portal}
          dark_mode={props.dark_mode}
        />
      )}
    </div>
  );
};

export default FilesView;
