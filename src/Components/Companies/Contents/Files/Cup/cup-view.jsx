import React, {useEffect} from "react";

import FileItems from "./FileItems";

import Styles from "./styles.module.css";

const CupView = props => {
  const without_uploading_file = props.list_files.filter(
    x =>
      !props.in_progress.find(
        y => y.file_id === x.id && y.status !== "complated",
      ),
  );
  const files = without_uploading_file.map((file, i) => {
    return (
      <FileItems
        file={file}
        key={file.id}
        checkedArrFun={props.checkedArrFun}
        download_file={props.download_file}
        dark_mode={props.dark_mode}
        checked_arr={props.checked_arr}
        window_width={props.window_width}
      />
    );
  });

  return (
    <div className="col h-100">
      <div
        className={`row h-100 d-flex  ${Styles.scroll} ${
          !props.dark_mode ? Styles.scroll_dark : ""
        }`}
      >
        {files}
      </div>
    </div>
  );
};

export default CupView;
