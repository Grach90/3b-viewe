import React, {useState, useEffect} from "react";
import {useSelector} from "react-redux";

import FileItemsView from "./file-items-view";

import {
  get_file_category,
  get_svf_file,
  get_image,
} from "./useEffect_functions";

const FileItemsContainer = props => {
  const [file_name, setFilename] = useState(props.file.filename);
  const [image, set_img] = useState("");
  const [window_width, setWindowWidth] = useState(0);
  const [file_category, set_file_category] = useState("");
  const [svfFile, set_svf_file] = useState("");

  const dark_mode = useSelector(state => state.dark_mode_reducer.mode);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
  }, [window.innerWidth]);

  useEffect(() => get_file_category(set_file_category, props.file), [
    props.file,
  ]);
  useEffect(() => get_svf_file(set_svf_file, props.file), [props.file]);

  useEffect(() => {
    get_image(svfFile, file_category, set_img);
  }, [file_category, svfFile]);

  useEffect(() => {
    setFilename(props.file.filename);
  }, [window.innerWidth, props.file]); // eslint-disable-line

  const forCheck = file => props.checkedArrFun(file);

  return (
    <FileItemsView
      name={file_name}
      file={props.file}
      checked_arr={props.checked_arr}
      forCheck={forCheck}
      download_file={props.download_file}
      dark_mode={dark_mode}
      window_width={window_width}
      image={image}
    />
  );
};

export default FileItemsContainer;
