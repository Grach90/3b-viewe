import ifcLogo from "static/images/ifcLogo.png";

export const get_file_category = (set_file_category, file) => {
  set_file_category(
    !!file.trees ? file.trees.filter(x => x.extension !== "svf")[0] : "",
  );
};
export const get_svf_file = (set_svf_file, file) => {
  set_svf_file(!!file.trees ? file.trees.find(x => x.extension === "svf") : "");
};
export const get_image = (svfFile, file_category, set_img) => {
  if (!!svfFile) {
    if (file_category.extension === "rvt") {
      const svf_name = svfFile.path.split("/").pop();
      const create_img_path = `${svf_name.split(".")[0]}4.png`;
      const changeInImg = svfFile.path.replace(svf_name, create_img_path);
      const imgPath = changeInImg.match(/storage.*/g)[0];
      set_img(`../../${imgPath}`);
    } else {
      set_img(ifcLogo);
    }
  }
};
