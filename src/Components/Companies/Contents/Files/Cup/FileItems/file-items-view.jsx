import React from "react";
import {Card, CardBody, CardImg} from "reactstrap";
import {CircularProgressbar} from "react-circular-progressbar";

import Styles from "./styles.module.css";
import "./download_files/styles.css";
import noneImg from "static/images/rvtLogo.png";

const FileItemsView = props => {
  if (!props.file) return null;
  return (
    <div
      className={
        props.window_width > 1450
          ? "col-3"
          : props.window_width > 1150
          ? "col-4"
          : props.window_width > 815
          ? "col-6"
          : props.window_width < 768 && props.window_width > 585
          ? "col-6"
          : "col-12"
      }
      style={
        window.window_width > 1600
          ? {
              width: "289px",
              height: "282px",
              padding: "0px",
            }
          : {
              width: window.window_width > 1024 ? "242px" : "232.62",
              height: window.window_width > 1024 ? "236.53px" : "226.99",
              padding: "0px",
            }
      }
    >
      <Card
        className={Styles.tableHead}
        style={{
          background: props.checked_arr.includes(props.file)
            ? props.dark_mode
              ? "#232323"
              : "#606060"
            : props.dark_mode
            ? "black"
            : "#B9B9B9",
          opacity: props.download_file[props.file.id]
            ? props.download_file[props.file.id].bool
              ? "0.2"
              : "1"
            : "1",
        }}
        onClick={() => props.forCheck(props.file)}
      >
        <label className={Styles.container}>
          <input
            type="checkbox"
            checked={props.checked_arr.includes(props.file)}
            onChange={() => props.forCheck(props.file)}
          />

          <span
            className={
              props.dark_mode ? Styles.checkmark : Styles.checkmarkLight
            }
          />
        </label>
        <CardImg
          top
          width="50%"
          src={props.image === "" ? noneImg : props.image}
          alt=""
          className={Styles.fotoModel}
        />
        <CardBody
          style={{padding: "0"}}
          className="d-flex justify-content-center"
        >
          <div
            className={Styles.description + " d-flex justify-content-between"}
            style={{
              color: props.checked_arr.includes(props.file)
                ? "white"
                : "#707070",
              height: "28px",
              width: "95%",
            }}
          >
            <div
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
                width: "calc(100% - 31px)",
              }}
              title={props.name}
            >
              {props.name}
            </div>
            <div
              style={{
                overflow: "hidden",
                textOverflow: "ellipsis",
              }}
            >
              V: {props.file.version}
            </div>
          </div>
          {props.download_file[props.file.id]
            ? props.download_file[props.file.id].bool && (
                <div
                  className="d-flex justify-content-center align-items-center"
                  style={{
                    float: "right",
                    position: "relative",
                    width: "100%",
                    height: "100%",
                    top:
                      props.window_width > 1800
                        ? "-180px"
                        : props.window_width > 1024
                        ? "-155px"
                        : props.window_width > 815
                        ? "-140px"
                        : props.window_width > 768
                        ? "-200px"
                        : props.window_width > 585
                        ? "-140px"
                        : "-155px",
                    borderRadius: "10px",
                  }}
                >
                  <div
                    className={props.dark_mode ? "popover1" : "popover2"}
                    style={{width: "120px", float: "right"}}
                  >
                    <CircularProgressbar
                      striped
                      value={
                        props.download_file[props.file.id].progressForDownload
                      }
                      text={`${
                        props.download_file[props.file.id].progressForDownload
                      }%`}
                    />
                  </div>
                </div>
              )
            : null}
        </CardBody>
      </Card>
    </div>
  );
};

export default FileItemsView;
