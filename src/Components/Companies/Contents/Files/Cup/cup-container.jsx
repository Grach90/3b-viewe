import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import CupView from "./cup-view";

// import {checkedArrFun} from "redux/files/files-actions";

const CupContainer = (props) => {
  return (
    <CupView
      // For files
      checkedArrFun={props.checkedArrFun}
      download_file={props.download_file}
      // From "../files-view"
      list_files={props.list_files}
      checked_arr={props.checked_arr}
      dark_mode={props.dark_mode}
      in_progress={props.in_progress}
      window_width={props.window_width}
    />
  );
};

CupContainer.propTypes = {
  socket: PropTypes.object,
  download_file: PropTypes.object,
  downin_progressload_file: PropTypes.array,
  checkedArrFun: PropTypes.func,
};

const mapStateToProps = (state) => {
  return {
    socket: state.io.socket,
    download_file: state.files.downloadFile,
    in_progress: state.files.in_progress,
    dark_mode: state.dark_mode_reducer.mode,
  };
};

// export default connect(mapStateToProps, {
//   // For files
//   checkedArrFun,
// })(CupContainer);

export default CupContainer;
