import React, { useState, useEffect } from "react";
// import { Redirect } from "react-router-dom";
import { Popover, PopoverBody } from "reactstrap";
import { FormattedMessage, useIntl } from "react-intl";
import { IProjectViewer } from "../types/ProjectsTypes";

import Folders from "./Folders";
// import Modal from "components/Portal";
import ModalYesNo from "../../Portal/yes_no_portal";
import messages from "../messages";
import AddModal from "./addModal/modal";

import Styles from "./styles.module.css";
import "./addPopover/addPopover.css";

import delBtnLight from "static/images/redo_grey.png";
import delBtn from "static/images/del.png";
import removeBtn from "static/images/baseline.png";
import removeBtnLight from "static/images/baselineLight.png";
import preloader from "static/preloader/Gear-0.2s-200px.svg";
import preloaderLight from "static/preloader/preloaderLight.svg";
import delBtnActive from "static/images/redo_white.png";
import markupCount from "static/images/markupCount.png";
import markupCountLight from "static/images/markupCountLight.png";
import issueCount from "static/images/issueIcon.png";
import issueCountLight from "static/images/issueIconLight.png";

const Projects: React.FC<IProjectViewer> = (props) => {
  const intl = useIntl();
  return (
    <div className="h-100">
      {/* {props.bool_for_redirect_to_new_project && (
        <Redirect
          to={`/${props.company_name}/projects/${props.project_name}`}
        />
      )}

      {props.bool_gl &&
        props.projects.length === props.checked_projects.length && (
          <Redirect to={`/${props.company_name}/projects`} />
        )} */}

      {/* {props.remove_proj &&
        props.projects.length !== 0 &&
        props.projects !== undefined &&
        props.projects !== null && (
          <div>
            {props.projects.length > 1 ? (
              props.projects.filter(
                (x) => !props.checked_projects.includes(x.id)
              ).length !== 0 ? (
                <Redirect
                  to={`/${props.company_name}/projects/${
                    props.projects.filter(
                      (x) => !props.checked_projects.includes(x.id)
                    )[props.next_index].name
                  }`}
                />
              ) : (
                <Redirect to={`/${props.company_name}/projects`} />
              )
            ) : (
              <Redirect to={`/${props.company_name}/projects`} />
            )}
          </div>
        )} */}

      <div
        className={`pt-4 w-100 ${
          props.dark_mode ? Styles.header : Styles.headerLight
        }`}
      >
        <h6 style={{ color: props.dark_mode ? "white" : "black" }}>
          <FormattedMessage {...messages.header} />
        </h6>
      </div>

      <div
        className={`w-100 ${
          props.dark_mode ? Styles.content : Styles.contentLight
        }`}
      >
        <div className={"col w-100 " + Styles.title}>
          <div className="row" style={{ height: "65%" }}>
            <h5
              className={"col-6 " + Styles.company_name}
              style={{ color: props.dark_mode ? "white" : "black" }}
            >
              {/* {props.company_name} */}
              Testbedrijf
            </h5>

            <div className={"col-6 " + Styles.pad}>
              <div
                className={"row mr-auto " + Styles.add}
                style={
                  {
                    // display: props.user_role === "3B Manager" ? "flex" : "none",
                  }
                }
              >
                <div
                  style={{ display: props.remove_buttons ? "flex" : "none" }}
                >
                  <button
                    value="Yes"
                    className={`d-flex justify-content-center align-items-center ${
                      props.dark_mode ? Styles.delete : Styles.deleteLight
                    } `}
                    title={intl.formatMessage({
                      id: "boilerplate.CompanyPage.deleteProject.title",
                    })}
                    onClick={() => props.open_partal_yes_no()}
                  >
                    <img
                      src={props.dark_mode ? removeBtn : removeBtnLight}
                      alt="X"
                    />
                  </button>

                  <button
                    value="no"
                    onClick={() => props.cancel()}
                    className={`${Styles.remove} justify-content-center align-items-center `}
                    style={{
                      backgroundColor: props.dark_mode ? "#2b2b2b" : "white",
                      border: props.dark_mode ? "none" : "1px solid #939394",
                    }}
                    title={intl.formatMessage({
                      id: "boilerplate.CompanyPage.deleteProject.cancel.title",
                    })}
                  >
                    <img
                      src={props.dark_mode ? delBtnActive : delBtnLight}
                      alt="X"
                    />
                  </button>
                </div>

                {props.remove_buttons || <AddModal />}

                <button
                  className={`${Styles.remove} justify-content-center align-items-center `}
                  id="addControlledPopover100"
                  onClick={() => props.changeValueOfRemoveButton(true)}
                  disabled={props.projects?.length === 0 ? true : false}
                  style={{
                    display: props.remove_buttons ? "none" : "flex",
                    backgroundColor: props.dark_mode ? "#2b2b2b" : "white",
                    border: props.dark_mode ? "none" : "1px solid #939394",
                  }}
                  title={intl.formatMessage({
                    id: "boilerplate.CompanyPage.deleteProject.title",
                  })}
                >
                  <img src={delBtn} alt="X" />
                </button>

                {/* {props.portal_bool && (
                  <Modal
                    portal_value={props.portal_value}
                    close_portal={props.close_portal}
                    dark_mode={props.dark_mode}
                  />
                )} */}
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col">
              <div className="row" style={{ paddingLeft: "21px" }}>
                <div
                  className={`${
                    props.dark_mode ? Styles.counts : Styles.countsLight
                  } d-flex justify-content-start align-items-center`}
                  title={intl.formatMessage({
                    id: "boilerplate.CompanyPage.markupsCount.title",
                  })}
                >
                  <img
                    src={props.dark_mode ? markupCount : markupCountLight}
                    alt=""
                  />

                  <div style={{ marginLeft: "6px" }}>
                    {/* {props.markup_pushin_counts !== undefined
                      ? !!props.markup_pushin_counts.markups_count
                        ? props.markup_pushin_counts.markups_count
                        : 0
                      : 0} */}
                  </div>
                </div>

                <div
                  className={`${
                    props.dark_mode ? Styles.counts : Styles.countsLight
                  } d-flex justify-content-start align-items-center`}
                  title={intl.formatMessage({
                    id: "boilerplate.CompanyPage.issuesCount.title",
                  })}
                >
                  <img
                    src={props.dark_mode ? issueCount : issueCountLight}
                    alt=""
                  />

                  {/* <div style={{ marginLeft: "6px" }}>
                    {props.markup_pushin_counts !== undefined
                      ? !!props.markup_pushin_counts.pushpins_count
                        ? props.markup_pushin_counts.pushpins_count
                        : 0
                      : 0}
                  </div> */}
                </div>
              </div>
            </div>
          </div>
        </div>

        <div //line
          style={{ height: "25px" }}
          className="d-flex justify-content-center align-items-center"
        >
          <hr
            className={Styles.line}
            style={{ backgroundColor: props.dark_mode ? "#707070" : "#C8C8C8" }}
          />
        </div>

        <div
          className={`col  ${Styles.folders} ${
            !props.dark_mode ? Styles.folder_light : ""
          }`}
        >
          {/* {props.isFetching && (
            <div className="w-100 d-flex justify-content-lg-center">
              <img
                className="d-flex justify-content-lg-center"
                src={props.dark_mode ? preloader : preloaderLight}
                style={{ width: "50px" }}
                alt=""
              />
            </div>
          )} */}
          {
            // props.projects.id !== null &&
            props.projects?.map((item, i) => (
              <Folders
                remove_buttons={props.remove_buttons}
                dark_mode={props.dark_mode}
                folder={item}
                key={item.id}
                set_bool_for_content={props.set_bool_for_content}
                projects={props.projects}
              />
            ))
          }
        </div>
      </div>

      {props.portal_bool_yes_no && (
        <ModalYesNo
          portal_value={props.portal_value_yes_no}
          close_portal={props.close_yes_no_portal}
          dark_mode={props.dark_mode}
          submit={props.confirmDeleteProjects}
        />
      )}
    </div>
  );
};

export default Projects;
