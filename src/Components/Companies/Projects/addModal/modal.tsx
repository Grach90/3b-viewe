import React, { useState } from "react";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";
import Styles from "../styles.module.css";
import addBtn from "static/images/add.png";
import { useIntl } from "react-intl";

const AddModal: React.FC = () => {
  const [modalShow, setModalShow] = useState(false);
  const [projectName, setProjectName] = useState("");
  const { company_id } = useTypedSelector(
    (state) => state.sessionState.session
  );
  const { createProject } = useActions();
  const intl = useIntl();
  return (
    <div>
      <button
        onClick={() => setModalShow(true)}
        className={`${Styles.addBtn} btn justify-content-center align-items-center`}
        title={intl.formatMessage({
          id: "boilerplate.CompanyPage.addProject",
        })}
      >
        <img src={addBtn} alt="+" />
      </button>
      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Complet the project configs.
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup>
            <FormControl
              placeholder="Project name"
              onChange={(e) => setProjectName(e.target.value)}
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button
            className={Styles.closeModalbtn}
            onClick={() => {
              createProject(company_id, projectName);
              setModalShow(false);
            }}
          >
            Create Project
          </Button>
          <Button
            className={Styles.closeModalbtn}
            onClick={() => setModalShow(false)}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default AddModal;
