export const get_project_name = (set_name, folder) => {
  const parent_with = document.getElementById("project_names")
    ? document.getElementById("project_names").offsetWidth
    : 200;

  const size = parseInt((parent_with * 7) / 100);
  const name =
    folder.name.length <= size
      ? folder.name
      : folder.name
          .split("")
          .map((x, i) => {
            if (i < size - 3) {
              return x;
            } else if (i === size || i === size - 1 || i === size - 2) {
              return ".";
            } else {
              return "";
            }
          })
          .join("");

  set_name(name);
};
