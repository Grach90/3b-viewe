// import React, { useEffect, useState } from "react";
// import { connect } from "react-redux";
// import { compose } from "redux";
// import { withRouter } from "react-router-dom";
// import PropTypes from "prop-types";
import React from "react";
import { IFolderContainer } from "../../types/ProjectsTypes";
import { useActions } from "hooks/useActions";
import { useTypedSelector } from "hooks/useTypedSelector";
import Folders from "./folders_view";

// import {
//   addProjectInCheckedArr,
//   getProjectConfigFile,
//   updateProject,
// } from "redux/projects/projects-actions";

// import { changePortal } from "redux/portal/portal-actions";

// import { get_project_name } from "./useEffect_functions";
const FoldersContainer: React.FC<IFolderContainer> = (props) => {
  const { addProjectInCheckedArr } = useActions();
  const { checkedProjects } = useTypedSelector((state) => state.projectState);
  const { company_id, company_name } = useTypedSelector(
    (state) => state.sessionState.session
  );
  // const [project_name, set_name] = useState(props.folder.name);

  // useEffect(() => {
  //   get_project_name(set_name, props.folder);
  // }, [
  //   document.getElementById("project_names"),
  //   props.folder.name,
  //   window.innerWidth,
  // ]);
  // const update_submit = (e, categores_arr) => {
  //   const project_configs = {
  //     project_id: props.folder.id,
  //     old_name: e.old_name,
  //     name: e.project_name,
  //     ils_point_3_1_project_name_parts: e.ils_point_3_1_project_name_parts,
  //     ils_point_3_3_template_for_level_names:
  //       e.ils_point_3_3_template_for_level_names,
  //     ils_point_4_1_template_for_space_property_name:
  //       e.ils_point_4_1_template_for_space_property_name,
  //     ils_point_4_8_template: categores_arr,
  //   };

  //   if (
  //     props.projects.find(
  //       x =>
  //         x.name === project_configs.name &&
  //         x.id !== project_configs.project_id,
  //     )
  //   ) {
  //     return props.changePortal(
  //       true,
  //       intl.formatMessage({id: "boilerplate.addProjectPortal.header"}),
  //     );
  //   }

  //   if (project_configs.name !== "") {
  //     props.updateProject(props.company_name, props.user_id, project_configs);
  //   }
  //   $(`#editProjectConfigsModal${props.folder.id}`).modal("toggle");
  // };

  return (
    <Folders
      company_name={company_name}
      // update_submit={update_submit}
      // location={props.location}
      folder={props.folder}
      remove_buttons={props.remove_buttons}
      addProjectInCheckedArr={addProjectInCheckedArr}
      checkedProjects={checkedProjects}
      dark_mode={props.dark_mode}
      company_id={company_id}
      // set_bool_for_content={props.set_bool_for_content}
      // project_name={project_name}
      // getProjectConfigFile={props.getProjectConfigFile}
      // project_configs={props.project_configs}
      // user={props.user}
    />
  );
};

// FoldersContainer.propTypes = {
//   company_name: PropTypes.string,
//   remove_buttons: PropTypes.bool,
//   checked_projects: PropTypes.array,
//   addProjectInCheckedArr: PropTypes.func,
//   project_configs: PropTypes.object,
// };

// const mapStateToProps = (state) => {
//   return {
//     company_name: state.session.user.company_names[0],
//     remove_buttons: state.projects.removeButtons,
//     checked_projects: state.projects.checkedProjects,
//     project_configs: state.projects.project_configs,
//     user: state.session.user,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// export default compose(
//   connect(mapStateToProps, {
//     addProjectInCheckedArr,
//     getProjectConfigFile,
//     updateProject,
//     changePortal,
//   }),
//   withRouter
// )(FoldersContainer);

export default FoldersContainer;
