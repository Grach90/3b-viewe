import React from "react";
import { NavLink } from "react-router-dom";

import { IFoldersView } from "../../types/ProjectsTypes";
import EditModal from "../editModal/EditModal";

import Styles from "./styles.module.css";
import open from "static/images/open.png";
import openLight from "static/images/openLight.png";

const Folders: React.FC<IFoldersView> = (props) => {
  return (
    <div
      className={`d-flex  ${
        props.dark_mode ? Styles.bigDiv : Styles.bigDivLight
      }`}
      onClick={() => {
        props.remove_buttons && props.addProjectInCheckedArr(props.folder.id);
      }}
    >
      <div
        className={Styles.container}
        style={{ display: props.remove_buttons ? "flex" : "none" }}
      >
        <input
          type="checkbox"
          onChange={() => {
            props.addProjectInCheckedArr(props.folder.id);
          }}
          checked={!!props.checkedProjects.find((i) => i === props.folder.id)}
        />

        <span
          className={props.dark_mode ? Styles.checkmark : Styles.checkmarkLight}
        />
      </div>
      {/* </div> */}
      <NavLink
        // onClick={() => {
        //   if (window.innerWidth <= 767) {
        //     props.set_bool_for_content(true);
        //   } else {
        //     props.set_bool_for_content(false);
        //   }
        // }}
        className={(navData) =>
          navData.isActive ? Styles.active : Styles.inactiveTab
        }
        to={`/${props.company_name}/projects/${props.folder.name}`}
        // to=""
        style={{
          pointerEvents: props.remove_buttons ? "none" : "auto",
          width: props.remove_buttons ? "calc(100% - 25px)" : "100%",
        }}
      >
        <div
          className={`${
            props.dark_mode ? Styles.menuItems : Styles.menuItemsLight
          } d-flex`}
          title={props.folder.name}
        >
          <div className={props.dark_mode ? Styles.icon : Styles.iconLights} />

          <div className={Styles.title}>
            {props.folder.name}
            <img
              src={props.dark_mode ? open : openLight}
              alt="open"
              className={Styles.openIcon}
            />
          </div>
        </div>
      </NavLink>
      {/* {props.user.role === "3B Manager" ||
      props.user.role === "Project Manager" ? ( */}
      {props.remove_buttons || (
        <EditModal
          project_id={props.folder.id}
          project_name={props.folder.name}
          dark_mode={props.dark_mode}
          company_id={props.company_id}
        />
      )}
      {/* ) : null} */}
    </div>
    // <NavLink
    //   // onClick={() => {
    //   //   if (window.innerWidth <= 767) {
    //   //     props.set_bool_for_content(true);
    //   //   }
    //   // }}
    //   to={`/${props.company_id}/${props.folder.name}`}
    //   // activeClassName={Styles.active}
    // >
    //   <div className={Styles.menuItems}>
    //     <div className={Styles.icon} />

    //     <div className={Styles.title}>
    //       loading
    //       <img
    //         src={props.dark_mode ? open : openLight}
    //         alt="open"
    //         className={Styles.openIcon}
    //       />
    //     </div>
    //   </div>
    // </NavLink>
  );
};

export default Folders;
