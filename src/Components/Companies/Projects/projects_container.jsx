import React, { useState, useEffect } from "react";
// import { connect } from "react-redux";
import { useIntl } from "react-intl";
// import { compose } from "redux";
// import { Navigate } from "react-router-dom";

import Projects from "./projects_view";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import { useActions } from "../../../hooks/useActions";

// import PropTypes from "prop-types";

// import io from "socket.io-client";

// import {
//   setSocket,
//   removeSocket,
//   setRoom,
//   removeRoom,
// } from "redux/socket/socket-actions";

// import {
//   createProject,
//   deleteProject,
//   fetchProjects,
//   changeValueOfRemoveButton,
//   emptyCheckedArr,
//   changeDeleteValue,
//   redirect_to_project,
//   set_value_for_failed_something_in_projects,
//   receiveUsersPermissions,
// } from "redux/projects/projects-actions";

// import { changePortal, changeYesNoPortal } from "redux/portal/portal-actions";

// import { message } from "antd";

// import {
//   get_move_another_project_after_deleteing,
//   get_project_id,
//   get_project_name,
//   connect_socket,
//   disconnect_socket,
//   connect_to_room,
//   leav_room,
//   get_name,
// } from "./useEffect_functions";

const ProjectsContainer = (props) => {
  const {
    changeValueOfRemoveButton,
    emptyCheckedArr,
    ChangeYesNoPortal,
    deleteProjects,
  } = useActions();
  const { removeButtons, items, checkedProjects, isFetching } =
    useTypedSelector((state) => state.projectState);
  console.log("ProjectsContainer", items);
  const { bool_yes_or_no, value_es_or_no } = useTypedSelector(
    (state) => state.portalState
  );
  const { company_id, company_name } = useTypedSelector(
    (state) => state.sessionState.session
  );
  const { fetchProjects } = useActions();

  // Projects
  // const [project_name, set_project_name] = useState("");
  // const [project_id, set_project_id] = useState("");
  // const [remove_proj, set_remove_project] = useState(false);

  // const [popover_open, set_popover_open] = useState(false);
  // const [bool_rm, set_bool_rm] = useState(false);
  // const [popover_delete, set_popover_delete] = useState(false);
  // const [bool_gl, set_bool_gl] = useState(false);

  // const [next_index, set_next_index] = useState(0);
  // const [
  //   projects_without_markup_pushpin_count,
  //   set_projects_without_markup_pushpin_count,
  // ] = useState([]);
  // const [markup_pushin_counts, set_markup_pushin_counts] = useState({});
  // const [name, set_name] = useState("");

  // const url =
  //   process.env.REACT_ENV === "production"
  //     ? `https://3bview.nl:${process.env.SERVER_PORT}`
  //     : "http://localhost:3004";

  // useEffect(() => {
  //   get_move_another_project_after_deleteing(
  //     set_next_index,
  //     props.projects,
  //     props.checked_projects
  //   );
  // }, [props.projects, props.checked_projects]);

  // useEffect(
  //   () =>
  //     set_projects_without_markup_pushpin_count(
  //       props.projects.filter((x) => !!x.id)
  //     ),
  //   [props.projects]
  // );

  // useEffect(() => {
  //   if (props.failed_something_in_projects.bool) {
  //     info(props.failed_something_in_projects.message);
  //   }
  // }, [props.failed_something_in_projects.bool]);

  // useEffect(() => {
  //   get_project_id(
  //     set_project_id,
  //     projects_without_markup_pushpin_count,
  //     props.match
  //   );
  // }, [projects_without_markup_pushpin_count]);

  // useEffect(() => {
  //   get_project_name(set_project_name, props.location);
  // }, [props.location.pathname]);

  // useEffect(() => {
  //   connect_socket(io, url, props.setSocket);

  //   return () => {
  //     disconnect_socket(io, url, props.removeSocket);
  //   };
  // }, []);

  // useEffect(() => {
  //   if (props.company_name !== "" && props.socket.on !== undefined) {
  //     const attrs = {
  //       company_name: props.company_name,
  //       user_id: props.user_id,
  //       socket: props.socket,
  //     };
  //     connect_to_room(attrs, props.setRoom);

  //     return () => {
  //       leav_room(attrs, props.removeRoom);
  //     };
  //   }
  // }, [props.company_name, props.socket]);

  // useEffect(() => {
  //   set_markup_pushin_counts(props.projects.find((x) => !x.id));
  // }, [props.projects]);

  useEffect(() => fetchProjects(company_id), []);
  // useEffect(() => set_remove_project(false), [remove_proj]);
  // useEffect(() => set_bool_rm(false), [bool_rm]);
  // useEffect(
  //   () => props.redirect_to_project(false),
  //   [props.bool_for_redirect_to_new_project]
  // );
  // useEffect(() => set_bool_gl(false), []);
  // useEffect(() => {
  //   get_name(set_name, projects_without_markup_pushpin_count, props.location);
  // }, [projects_without_markup_pushpin_count, props.location]);

  // useEffect(() => {
  //   if (project_id) {
  //     props.receiveUsersPermissions(props.user.id, project_id);
  //   }
  // }, [project_id]);

  // const toggle = () => set_popover_open(!popover_open);
  const intl = useIntl();

  // function info(text) {
  //   message.warning(text);
  //   return props.set_value_for_failed_something_in_projects(false, "");
  // }

  // const create_submit = (e, categores_arr) => {
  //   const project_configs = {
  //     name: e.project_name,
  //     ils_point_3_1_project_name_parts: e.ils_point_3_1_project_name_parts,
  //     ils_point_3_3_template_for_level_names:
  //       e.ils_point_3_3_template_for_level_names,
  //     ils_point_4_1_template_for_space_property_name:
  //       e.ils_point_4_1_template_for_space_property_name,
  //     ils_point_4_8_template: categores_arr,
  //   };
  //   const proj_names = projects_without_markup_pushpin_count.map((x) => x.name);
  //   if (proj_names.includes(project_configs.name)) {
  //     return props.changePortal(
  //       true,
  //       intl.formatMessage({ id: "boilerplate.addProjectPortal.header" })
  //     );
  //   }
  //   if (project_configs.name !== "") {
  //     set_project_name(project_configs.name);
  //     props.createProject(
  //       props.company_name,
  //       props.user_id,
  //       project_configs,
  //       project_id
  //     );
  //   }
  //   $("#newProjectConfigsModal").modal("toggle");
  // };

  // const remove_submit = async (e) => {
  //   props.changeYesNoPortal(false);
  //   set_popover_delete(!popover_delete);
  //   set_bool_rm(!bool_rm);

  //   let array = props.checked_projects;

  //   if (props.checked_projects.length !== 0) {
  //     if (array.length !== props.checked_projects.length) {
  //       return props.changePortal(
  //         true,
  //         intl.formatMessage({ id: "boilerplate.cantDeletePortal.header" })
  //       );
  //     }

  //     if (projects_without_markup_pushpin_count.length === array.length) {
  //       props.changeDeleteValue(10);
  //       set_bool_gl(true);
  //     }

  //     set_remove_project(true);

  //     await props.deleteProject(props.user_id, array, props.company_name);
  //   } else {
  //     return props.changePortal(
  //       true,
  //       intl.formatMessage({ id: "boilerplate.emptyDelete.header" })
  //     );
  //   }

  //   props.changeValueOfRemoveButton();
  //   props.emptyCheckedArr();
  // };

  const cancel = () => {
    changeValueOfRemoveButton(false);
    emptyCheckedArr();
  };

  // const close_portal = () => props.changePortal(false);
  const close_yes_no_portal = () => ChangeYesNoPortal(false);

  const open_partal_yes_no = () => {
    // if (props.checked_projects.length === 0) {
    //   return props.changePortal(
    //     true,
    //     intl.formatMessage({ id: "boilerplate.emptyDelete.header" })
    //   );
    // }

    return ChangeYesNoPortal(
      true,
      intl.formatMessage({ id: "boilerplate.sureToDelete.header" })
      // remove_submit
    );
  };

  const confirmDeleteProjects = () => {
    ChangeYesNoPortal(false);
    deleteProjects(company_id, checkedProjects);
    // set_remove_project(true);
  };
  return (
    <div className="h-100">
      <Projects
        projects={props.projects}
        changeValueOfRemoveButton={changeValueOfRemoveButton}
        remove_buttons={removeButtons}
        cancel={cancel}
        portal_bool_yes_no={bool_yes_or_no}
        portal_value_yes_no={value_es_or_no}
        close_yes_no_portal={close_yes_no_portal}
        open_partal_yes_no={open_partal_yes_no}
        confirmDeleteProjects={confirmDeleteProjects}
        isFetching={isFetching}
        // create_submit={create_submit}
        // popover_open={popover_open}
        // toggle={toggle}
        // company_name={props.company_name}
        // markup_pushin_counts={markup_pushin_counts}
        // project_name={project_name}
        // remove_proj={remove_proj}
        // name={name}
        // checked_projects={props.checked_projects}
        // portal_bool={props.portal_bool}
        // portal_value={props.portal_value}
        // close_portal={close_portal}
        // bool_gl={bool_gl}
        // dark_mode={props.dark_mode}
        // function_for_submit={props.function_for_submit}
        // set_bool_for_content={props.set_bool_for_content}
        // bool_for_redirect_to_new_project={
        //   props.bool_for_redirect_to_new_project
        // }
        // failed_something_in_projects={props.failed_something_in_projects}
        // set_value_for_failed_something_in_projects={
        //   props.set_value_for_failed_something_in_projects
        // }
        // next_index={next_index}
        // user_role={props.user.role}
      />
    </div>
  );
};

// ProjectsContainer.propTypes = {
//   projects: PropTypes.array,
//   user_id: PropTypes.string,
//   company_name: PropTypes.string,
//   is_fetching: PropTypes.bool,
//   bool_for_redirect_to_new_project: PropTypes.bool,
//   remove_buttons: PropTypes.bool,
//   checked_projects: PropTypes.array,
//   failed_something_in_projects: PropTypes.object,
//   portal_bool: PropTypes.bool,
//   portal_bool_yes_no: PropTypes.bool,
//   portal_value: PropTypes.string,
//   portal_value_yes_no: PropTypes.string,
//   function_for_submit: PropTypes.func,
//   socket: PropTypes.object,
//   fetchProjects: PropTypes.func,
//   createProject: PropTypes.func,
//   deleteProject: PropTypes.func,
//   changeValueOfRemoveButton: PropTypes.func,
//   emptyCheckedArr: PropTypes.func,
//   changePortal: PropTypes.func,
//   changeDeleteValue: PropTypes.func,
//   changeYesNoPortal: PropTypes.func,
//   setSocket: PropTypes.func,
//   removeSocket: PropTypes.func,
//   setRoom: PropTypes.func,
//   removeRoom: PropTypes.func,
//   redirect_to_project: PropTypes.func,
//   set_value_for_failed_something_in_projects: PropTypes.func,
// };

// const mapStateToProps = state => {
//   return {
//     projects: state.projects.items,
//     user: state.session.user,
//     user_id: state.session.user.id,
//     company_name: state.session.user.company_names[0],
//     is_fetching: state.projects.isFetching,
//     bool_for_redirect_to_new_project:
//       state.projects.bool_for_redirect_to_new_project,
//     remove_buttons: state.projects.removeButtons,
//     checked_projects: state.projects.checkedProjects,
//     failed_something_in_projects: state.projects.failed_something_in_projects,
//     portal_bool: state.portal.bool,
//     portal_value: state.portal.value,
//     portal_bool_yes_no: state.portal.bool_yes_or_no,
//     portal_value_yes_no: state.portal.value_es_or_no,
//     function_for_submit: state.portal.function_for_submit,
//     // Socket
//     socket: state.io.socket,
//     dark_mode: state.dark_mode_reducer.mode,
//   };
// };

// compose(
//   connect(mapStateToProps, {
//     fetchProjects,
//     createProject,
//     deleteProject,
//     changeValueOfRemoveButton,
//     emptyCheckedArr,
//     changePortal,
//     changeDeleteValue,
//     changeYesNoPortal,
//     // Sockets
//     setSocket,
//     removeSocket,
//     setRoom,
//     removeRoom,
//     //
//     redirect_to_project,
//     set_value_for_failed_something_in_projects,
//     receiveUsersPermissions,
//   }),
//   withRouter,
// )(ProjectsContainer);

export default ProjectsContainer;
