export const get_move_another_project_after_deleteing = (
  set_next_index,
  projects,
  checked_projects,
) => {
  const index = projects
    .map((x, i) => (checked_projects.find(y => y.id === x.id) ? i : ""))
    .filter(x => x !== "");

  set_next_index(index[0] === 0 ? 0 : index[0] - 1);
};

export const get_project_id = (
  set_project_id,
  projects_without_markup_pushpin_count,
  match,
) => {
  if (projects_without_markup_pushpin_count.length !== 0) {
    const project = projects_without_markup_pushpin_count.find(
      x => x.name === match.params.project_name,
    );

    if (project !== undefined) {
      return set_project_id(project.id);
    }
  }
};

export const get_project_name = (set_project_name, location) => {
  if (location.pathname.split("/").length <= 3) return;

  const project_name = location.pathname.split("/").pop();
  set_project_name(project_name);
};

export const connect_socket = (io, url, setSocket) => {
  const socket = io.connect(url, {transports: ["websocket"]});
  setSocket(socket);

  console.log("Connecting socket...");
};

export const disconnect_socket = (io, url, removeSocket) => {
  const socket = io.connect(url, {transports: ["websocket"]});
  socket.disconnect();
  removeSocket();
  console.log("Disconnecting socket...");
};

export const connect_to_room = ({company_name, user_id, socket}, setRoom) => {
  const room = JSON.stringify({
    company_name: company_name,
    user_id: user_id,
  });

  setRoom(room);
  socket.on("connect", () => console.log("socket connection established..."));

  socket.emit("join", room);
  console.log(`Socket joining to the room.`);
};

export const leav_room = ({company_name, user_id, socket}, removeRoom) => {
  const room = JSON.stringify({
    company_name: company_name,
    user_id: user_id,
  });

  socket.emit("leave", room);
  removeRoom(room);

  console.log(`Socket leave from the room.`);
};

export const get_name = (
  set_name,
  projects_without_markup_pushpin_count,
  location,
) => {
  set_name(
    projects_without_markup_pushpin_count.length !== 0
      ? location.pathname.split("/")[3]
      : "",
  );
};
