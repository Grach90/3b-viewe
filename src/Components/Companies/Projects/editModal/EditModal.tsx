import React, { useState } from "react";
import { useActions } from "hooks/useActions";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";
import { IEditModalProps } from "../../types/ProjectsTypes";
import Styles from "../styles.module.css";
import editBtn from "static/images/icons8-edit-150 (7).png";
import { useIntl } from "react-intl";

const EditModal: React.FC<IEditModalProps> = (props) => {
  const [modalShow, setModalShow] = useState(false);
  const [projectName, setProjectName] = useState(props.project_name);
  const { updateProject } = useActions();
  const intl = useIntl();
  return (
    <div className="m-auto">
      <button
        onClick={() => setModalShow(true)}
        className={`${Styles.editBtn} btn justify-content-center align-items-center`}
        title={intl.formatMessage({
          id: "boilerplate.CompanyPage.addProject",
        })}
      >
        <img src={editBtn} alt="+" />
      </button>
      <Modal
        show={modalShow}
        onHide={() => setModalShow(false)}
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
            Complet the project configs.
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <InputGroup>
            <FormControl
              placeholder="Project name"
              onChange={(e) => setProjectName(e.target.value)}
              value={projectName}
            />
          </InputGroup>
        </Modal.Body>
        <Modal.Footer>
          <Button
            className={Styles.closeModalbtn}
            onClick={() => {
              setProjectName(props.project_name);
              updateProject(props.company_id, props.project_id, projectName);
            }}
          >
            Save
          </Button>
          <Button
            className={Styles.closeModalbtn}
            onClick={() => {
              setProjectName(props.project_name);
              setModalShow(false);
            }}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default EditModal;
