import { IProject } from "redux/projects/ProjectTypes";
import {
  addProjectInCheckedArr,
  changeValueOfRemoveButton,
  deleteProjects,
} from "redux/projects/ProjectActions";
import { ChangeYesNoPortal } from "redux/portal/PortalActions";

export interface IEditModalProps {
  project_name: string;
  dark_mode: boolean;
  project_id: string;
  company_id: string;
}

export interface IFoldersView {
  company_name: string;
  folder: IProject;
  remove_buttons: boolean;
  addProjectInCheckedArr: typeof addProjectInCheckedArr;
  checkedProjects: Array<string>;
  dark_mode: boolean;
  company_id: string;
}

export interface IFolderContainer {
  remove_buttons: boolean;
  dark_mode: boolean;
  folder: IProject;
  set_bool_for_content: boolean;
  projects: Array<IProject>;
}

export interface IProjectViewer {
  set_bool_for_content: boolean;
  dark_mode: boolean;
  projects: Array<IProject>;
  remove_buttons: boolean;
  portal_bool_yes_no: boolean;
  portal_value_yes_no: string;
  isFetching: boolean;
  cancel(): void;
  changeValueOfRemoveButton: typeof changeValueOfRemoveButton;
  close_yes_no_portal: () => typeof ChangeYesNoPortal;
  open_partal_yes_no: () => typeof ChangeYesNoPortal;
  confirmDeleteProjects(): void;
}
