import React, { useEffect, useState } from "react";

import Contents from "./Contents";
import Projects from "./Projects";

import Styles from "./styles.module.css";

const Companies = (props) => {
  console.log("Companies");
  const [bool_for_content, set_bool_for_content] = useState(false);
  const [window_width, setWindowWidth] = useState(0);

  useEffect(() => {
    setWindowWidth(window.innerWidth);
  }, [window.innerWidth]);

  return (
    <div className={"container h-100 " + Styles.content}>
      <div className={"row " + Styles.projects}>
        <div
          id="project_names"
          className={"col-md-4 col-lg-3  h-100 " + Styles.projectContainer}
          style={{
            display: bool_for_content && window_width <= 767 ? "none" : "block",
          }}
        >
          <Projects
            projects={props.projects}
            // set_bool_for_content={set_bool_for_content}
            // dark_mode={props.dark_mode}
          />
        </div>
        <div
          style={{ width: "100%", position: "relative" }}
          // className={"col-md-8  col-lg-9 h-100 " + Styles.projectContent}
          // style={{
          //   display:
          //     window_width <= 767
          //       ? !bool_for_content
          //         ? "none"
          //         : "block"
          //       : "block",
          // }}
        >
          <Contents
            set_bool_for_content={set_bool_for_content}
            dark_mode={props.dark_mode}
          />
        </div>
      </div>
    </div>
  );
};

export default Companies;
